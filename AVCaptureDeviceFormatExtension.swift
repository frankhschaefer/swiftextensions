//
//  AVCaptureDeviceFormat.swift
//  lapapp
//
//  Created by Frank Schäfer on 11.11.17.
//  Copyright © 2017 Frank Schäfer. All rights reserved.
//

import Foundation
import AVFoundation

extension AVCaptureDevice.Format {
    
    func resolution () -> Int32 {
        
        let dimensions = CMVideoFormatDescriptionGetDimensions(self.formatDescription)
        
        return dimensions.height * dimensions.width
        
    }
    
}
