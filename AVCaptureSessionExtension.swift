//
//  AVCaptureSessionExtension.swift
//  lapapp
//
//  Created by Frank Schäfer on 10.11.17.
//  Copyright © 2017 Frank Schäfer. All rights reserved.
//

import Foundation
import AVFoundation

/**
 Debug support function. List all available AVCaptureSessionPresets for this device / setup
 
 - parameter avCaptureSession: valid / open AVCaptureSession
 */
extension AVCaptureSession {
    
    /// Sorted by quality
    var presets : [AVCaptureSession.Preset] {
        
        get {
            return  [AVCaptureSession.Preset.hd4K3840x2160,
                     AVCaptureSession.Preset.hd1920x1080,
                     AVCaptureSession.Preset.hd1280x720,
                     AVCaptureSession.Preset.high,
                     AVCaptureSession.Preset.iFrame1280x720,
                     AVCaptureSession.Preset.medium,
                     AVCaptureSession.Preset.iFrame960x540,
                     AVCaptureSession.Preset.vga640x480,
                     AVCaptureSession.Preset.cif352x288,
                     AVCaptureSession.Preset.inputPriority,
                     AVCaptureSession.Preset.low,
                     AVCaptureSession.Preset.photo
                    ]
        }
        
    } //  var
    
    
    
    func listAllSupportedAVSessions() {
        
        for preset in presets {
            if self.canSetSessionPreset(preset) {
                        LoggerHandler.sharedLogger.log(lineNumber: #line,
                                           method: #function,
                                           file: #file,
                                           level: .info,
                                           tag: "POSITION",
                                           message:"yes for : \( preset)")
            } else {
                        LoggerHandler.sharedLogger.log(lineNumber: #line,
                                           method: #function,
                                           file: #file,
                                           level: .info,
                                           tag: "POSITION",
                                           message:"no for : \( preset)")
            }
        }
        
    }  // func
    
    
    
    /// Remove all inout and output streams
    func removeInputOutput () {
        
        for input in self.inputs {
            self.removeInput(input)
        }
        
        
        for output in self.outputs {
            self.removeOutput(output)
        }
        
    }
    
    
    /// Preset with the best resolutition
    func getBestPreset(avCaptureDevice  : AVCaptureDevice) -> AVCaptureSession.Preset? {
        
        for preset in presets {
            if self.canSetSessionPreset(preset) &&
               avCaptureDevice.supportsSessionPreset(preset) {
                
                return preset
            }
        }

       LoggerHandler.sharedLogger.log(lineNumber: #line,
                                      method: #function,
                                      file: #file,
                                      level: .warning,
                                      tag: "SETTING",
                                      message: "getBestPreset == nil")
        
        return nil
        
    }  // func

    
    
} // extension


