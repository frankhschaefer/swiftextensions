//
//  phAssertExtension.swift
//  lapapp
//
//  Created by Frank Schäfer on 30.12.15.
//  Copyright © 2015 Frank Schäfer. All rights reserved.
//
// Based on a soulution from stackoverflow
// http://stackoverflow.com/questions/27854937/ios8-photos-framework-how-to-get-the-nameor-filename-of-a-phasset


import Foundation
import Photos

// MARK: - This supprts the filename from an measurement. This name is used in the CSV-Export
extension PHAsset {
    
    var originalFilename : String? {
        
        var fname : String?
        
        let resources = PHAssetResource.assetResources(for: self)
        
        if let resource = resources.first {
            fname = resource.originalFilename
        }
        
        
        if fname == nil {
            // this is an undocumented workaround that works as of iOS 9.1
            fname = self.value(forKey: "filename") as? String
        }
        
        return fname
    }
}

