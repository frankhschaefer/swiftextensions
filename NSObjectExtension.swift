
//
//  NSAttributedStringExtension.swift
//  lapapp
//
//  Created by Frank Schäfer on 02.09.18.
//  Copyright © 2018 Frank Schäfer. All rights reserved.
//

import UIKit

extension NSObject {
    var theClassName: String {
        return NSStringFromClass(type(of: self))
    }
}

class Utility{
    class func classNameAsString(obj: Any) -> String {
        //prints more readable results for dictionaries, arrays, Int, etc
        return String(describing: type(of: obj))
    }
}

