//
//  AVCapturePhotoOutput_Extension.swift
//  lapapp
//
//  Created by Frank Schäfer on 23.04.17.
//  Copyright © 2017 Frank Schäfer. All rights reserved.
//

import Foundation
import Photos
import UIKit

/**
 
 */

struct PictureMetaData {
    
    var detectModus : DetectModus
    var infoText    : String
    var isInit      : Bool
    
    init(detectModus    : DetectModus = .unknown,
         infoText       : String = "") {
        
        self.isInit = true
        self.detectModus = detectModus
        self.infoText = infoText
    }
    
    var debugInfo : String {
        get {
            return "isInit: \(isInit), " +
                   "detectModus : \(detectModus), " +
                   "infoText \(infoText)"
        }
    }
}


extension AVCapturePhotoOutput {


    private struct CustomProperties {
        static var uniqueID         : String = NSUUID().uuidString
        static var pictureMetaData  : PictureMetaData = PictureMetaData.init()
        
        var debugInfo : String {
            get {
                return "uniqueID: \(CustomProperties.uniqueID), " +
                        "pictureMetaData : \(CustomProperties.pictureMetaData.debugInfo)"
            }
        }

    }  // struct
    
    
    var debugInfo : String {
    
    get {
            return CustomProperties().debugInfo + ", \( (CustomProperties.uniqueID))"
        }
    }

                                                           
    var pictureMetaData: PictureMetaData {
        get {
            
            print("Get , pictureMetaData : \(CustomProperties.uniqueID), \(debugInfo)")
            
            return objc_getAssociatedObject(self,
                                            &CustomProperties.uniqueID) as! PictureMetaData
        }
    
        set {
            
            print("Set, pictureMetaData : \(CustomProperties.uniqueID), \(debugInfo)")
            
            objc_setAssociatedObject(
                self,
                &CustomProperties.uniqueID,
                newValue as PictureMetaData,
                .OBJC_ASSOCIATION_RETAIN_NONATOMIC
            )
        }
    }  // var

    
}  // extension

