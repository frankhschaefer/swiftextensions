//
//  DateExtension.swift
//  lapapp
//
//  Created by Frank Schäfer on 29.12.17.
//  Copyright © 2017 Frank Schäfer. All rights reserved.
//

import Foundation

extension Date {
    
    var millisecondsSince1970:Int64 {
        return Int64((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds:Int64) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds / 1000))
    }

    /// Returns a String from the current date and time like "2017-12-29 18:47:12.1234"
    var dateWithMilliseconds : String {
        

        let df = DateFormatter()
        df.dateFormat = "y-MM-dd HH:mm:ss.SSSS"
    
        return df.string(from: self) //
    
    }
    
    /**
     
     let dateFormatter = DateFormatter()
     dateFormatter.dateFormat = "yyyy-MM-dd"
     let start = dateFormatter.date(from: "2018-01-01")!
     let end = dateFormatter.date(from: "2019-01-01")!
     
     let diff = Date.daysBetween(start: start, end: end)
     => 365
     
     - parameter date: end date
     
     - returns: Difference in days

     */
    func daysBetween(date: Date) -> Int {
        return Date.daysBetween(start: self, end: date)
    }
    
    
    /**
     
     let dateFormatter = DateFormatter()
     dateFormatter.dateFormat = "yyyy-MM-dd"
     let start = dateFormatter.date(from: "2018-01-01")!
     let end = dateFormatter.date(from: "2019-01-01")!
     
     let diff = Date.daysBetween(start: start, end: end)
     => 365
     
     - parameter start: start date

     - parameter end: end date

     - returns: Difference in days
     
     */
   static func daysBetween(start: Date, end: Date) -> Int {
        let calendar = Calendar.current
        
        // Replace the hour (time) of both dates with 00:00
        let date1 = calendar.startOfDay(for: start)
        let date2 = calendar.startOfDay(for: end)
        
        let a = calendar.dateComponents([.day], from: date1, to: date2)
        
        return a.value(for: .day)!
    }
    
    
   /**
       
       let dateFormatter = DateFormatter()
       dateFormatter.dateFormat = "yyyy-MM-dd"
       let start = dateFormatter.date(from: "2018-01-01 20:00:00")!
       let end = dateFormatter.date(from: "2019-01-01 20:00:01")!
       
       let diff = Date.daysBetween(start: start, end: end)
       => 60
       
       - parameter date: end date
       
       - returns: Difference in secs
       
       */
     static func secondsBetween(start: Date, end: Date) -> Int {
          let calendar = Calendar.current
          
          // Replace the hour (time) of both dates with 00:00
          let date1 = calendar.startOfDay(for: start)
          let date2 = calendar.startOfDay(for: end)
          
          let a = calendar.dateComponents([.second], from: date1, to: date2)
          
          return a.value(for: .second)!
      }
      
     
    /**
     
     let dateFormatter = DateFormatter()
     dateFormatter.dateFormat = "yyyy-MM-dd"
     let start = dateFormatter.date(from: "2018-01-01 20:00:00")!
     let end = dateFormatter.date(from: "2019-01-01 20:00:01")!
     
     let diff = Date.daysBetween(start: start, end: end)
     => 60
     
     - parameter date: end date
     
     - returns: Difference in secs

     */
    func secondsBetween(date: Date) -> Int {
        return Date.secondsBetween(start: self, end: date)
    }
    

    func stringFromTimeWithSecondsInUTCFormat() -> String {
        
        let utcDateFormatter = DateFormatter()
        utcDateFormatter.dateStyle = .none
        utcDateFormatter.timeStyle = .medium

        // The default timeZone on DateFormatter is the device’s
        // local time zone. Set timeZone to UTC to get UTC time.
        utcDateFormatter.timeZone = TimeZone(abbreviation: "UTC")

        return utcDateFormatter.string(from: self)
    }

    
    func stringFromDateInUTCFormat() -> String {
        
        let utcDateFormatter = DateFormatter()
        utcDateFormatter.dateStyle = .medium
        utcDateFormatter.timeStyle = .none

        // The default timeZone on DateFormatter is the device’s
        // local time zone. Set timeZone to UTC to get UTC time.
        utcDateFormatter.timeZone = TimeZone(abbreviation: "UTC")

        return utcDateFormatter.string(from: self)
    }

    
    
    func stringFromDate() -> String {
        let timestamp = DateFormatter.localizedString(from: self, dateStyle: .medium, timeStyle: .none)
        return timestamp
    }
    
    
    func stringFromTime() -> String {
        let timestamp = DateFormatter.localizedString(from: self, dateStyle: .none, timeStyle: .short)
        return timestamp
    }
    
    
    func stringFromTimeWithSeconds() -> String {
        
        let timestamp = DateFormatter.localizedString(from: self, dateStyle: .none, timeStyle: .medium)
        return timestamp
        
    }
    
    func isLessThanDate(dateToCompare: Date) -> Bool {
        
        //Declare Variables
        var isLess = false

        //Compare Values
        if self.compare(dateToCompare as Date) == ComparisonResult.orderedAscending {
            isLess = true
        }

        //Return Result
        return isLess
    }


    func isGreaterThanDate(dateToCompare: Date) -> Bool {
        //Declare Variables
        var isGreater = false

        //Compare Values
        if self.compare(dateToCompare as Date) == ComparisonResult.orderedDescending {
            isGreater = true
        }

        //Return Result
        return isGreater
    }


    func startOfMonth() -> Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self)))!
    }
    
    func endOfMonth() -> Date {
        return Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: self.startOfMonth())!
    }

}
