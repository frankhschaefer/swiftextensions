//
//  DoubleExtension.swift
//  lapapp
//
//  Created by Frank Schäfer on 22.04.18.
//  Copyright © 2018 Frank Schäfer. All rights reserved.
//

import Foundation

// Create a string with currency formatting based on the device locale
//
extension Float {
    
    /// returns a String with 2-digits in the locale style (UI-usage)
    var asLocaleDecimalStyle    :   String {
        
        let formatter         = NumberFormatter()
        
        formatter.numberStyle = .decimal
        
        formatter.locale      = Locale.current
        
        return formatter.string(from: self as NSNumber)!
        
    }
    
    
    /// returns a String with n-digits
    func string(fractionDigits:Int) -> String {
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = fractionDigits
        formatter.maximumFractionDigits = fractionDigits
        return formatter.string(from: NSNumber(value: self)) ?? "\(self)"
    }
    
    /**
     returns the value between the limits or the boarder values
    */
    public func limit(minimalValue : Float, maximalValue : Float) -> Float {
        if self < minimalValue {
            return minimalValue
        }
        if self > maximalValue {
            return maximalValue
        }
        return self
    }
    

    /**
     Set the value between the limits or the boarder values
     */
    public init (minimalValue : Float, value : Float, maximalValue : Float) {
        if value < minimalValue {
            self = minimalValue
            return
        }
        if value > maximalValue {
            self = maximalValue
            return
        }
        self = value
        
    }
    
    /// SwiftRandom extension
    public static func random(_ lower: Float = 0, _ upper: Float = 100) -> Float {
        return  Float.random(in: lower...upper)
    }
    
    /// Radian to degree = RAD * 180 / PI
    public func radToDegree() -> Float {
        return self * 180 / Float.pi
    }
    
    
    /// Radian to degree = RAD * 180 / PI
    public func degreeToRad() -> Float {
        return self *  Float.pi / 180
    }

    
    func roundedDecimal(fractionDigits: Int = 0) -> Float {
        
        let strValue = self.string(fractionDigits: fractionDigits)

        let value = (strValue as NSString).doubleValue
        
        return Float(value)
    }
    
}

