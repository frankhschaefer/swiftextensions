//
//  CGFloatExtension.swift
//  lapapp
//
//  Created by Frank Schäfer on 17.12.17.
//  Copyright © 2017 Frank Schäfer. All rights reserved.
//

import Foundation
import UIKit


extension CGFloat {
    
    
    func validate(min : CGFloat, max : CGFloat) -> CGFloat {
        
        var validated : CGFloat = self
        
        if validated > max {
            // debugPrint("validate max, current value: \(validated), against \(max)")
            validated = max
        }
        
        if validated < min {
            // debugPrint("validate min, current value: \(validated), against \(min)")
            validated = min
        }

        return validated
        
    }
    
    /// returns a String with n-digits
    func string(fractionDigits:Int) -> String {
            return String(format:"%.\(fractionDigits)f", self)
    }
    
        
    /// SwiftRandom extension
    public static func random(_ lower: CGFloat = 0, _ upper: CGFloat = 1) -> CGFloat {
        return CGFloat(Float(arc4random()) / Float(UINT32_MAX)) * (upper - lower) + lower
    }

    
    /// Radian to degree = RAD * 180 / PI
    public func radToDegree() -> CGFloat {
        return self * 180 / CGFloat.pi
    }
    
    
    /// Radian to degree = RAD * 180 / PI
    public func degreeToRad() -> CGFloat {
        return self *  CGFloat.pi / 180
    }
    
    var int32 : Int32
    {
        return Int32(self.rounded())
    }
    
    
    
    func roundedDecimal(fractionDigits: Int = 0) -> CGFloat {
        
        let strValue = self.string(fractionDigits: fractionDigits)

        let value = (strValue as NSString).doubleValue
        
        return CGFloat(value)
    }
    
}
