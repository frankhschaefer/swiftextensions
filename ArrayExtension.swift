//
//  ArrayExtension.swift
//  lapapp
//
//  Created by Frank Schäfer on 04.01.18.
//  Copyright © 2018 Frank Schäfer. All rights reserved.
//

import Foundation

extension Array where Element: Equatable {
    @discardableResult
    mutating func appendIfNotContains(_ element: Element) -> (appended: Bool, memberAfterAppend: Element) {
        if let index = firstIndex(of: element) {
            return (false, self[index])
        } else {
            append(element)
            return (true, element)
        }
    }
}

extension Array {
    
    /// usage: array.item[safe: 1]
    /// Returns element or nil
    subscript (safe index: Int) -> Element? {

        if indices.count == 0 ||
           self.count == 0 ||
           self.isEmpty {
            return nil
        }
        
        return indices ~= index ? self[index] : nil
    }
    
    /// Remove all duplicates elements
    /// usage: let withoutDuplicates = searchResults.removingDuplicates(byKey: { $0.index })
    func removingDuplicates<T: Hashable>(byKey key: (Element) -> T)  -> [Element] {
        var result = [Element]()
        var seen = Set<T>()
        for value in self {
            if seen.insert(key(value)).inserted {
                result.append(value)
            }
        }
        return result
    }
    
    
    /// Remove this object from the array
    mutating func removeObject<U: Equatable>(_ object: U) {
        var index: Int?
        for (idx, objectToCompare) in self.enumerated() {
            if let to = objectToCompare as? U {
                if object == to {
                    index = idx
                }
            }
        }
            
        if((index) != nil) {
            self.remove(at: index!)
        }
    }
    
    /// Search for an element in the array
    /// Returns the index of this element or nil
    func find(_ includedElement: (Element) -> Bool) -> Int? {
        for (idx, element) in self.enumerated() {
            if includedElement(element) {
                return idx
            }
        }
        return nil
    }
}

/// This allows the remoing of an object from arrays
/// Sample
/// var myArray = ["Dog", "Cat", "Ant", "Fish", "Cat"]
/// myArray = myArray.arrayRemovingObject("Cat" )
extension Array where Element: Equatable {
    func arrayRemovingObject(_ object: Element) -> [Element] {
        return filter { $0 != object }
    }
}

extension Array where Element == Int {
    
    func median() -> Double {
        let sortedArray = sorted()
        if count % 2 != 0 {
            return Double(sortedArray[count / 2])
        } else {
            return Double(sortedArray[count / 2] + sortedArray[count / 2 - 1]) / 2.0
        }
    }
    
    func sum() -> Int {
        
        return self.reduce(0, +)
    }

    func mean() -> Double {
        
        guard self.count > 0 else {
            return 0
        }
        
        return Double(sum()) / Double(self.count)
        
    }

    
}

extension Array where Element: Hashable {
    func duplicates() -> Array {
        let groups = Dictionary(grouping: self, by: {$0})
        let duplicateGroups = groups.filter {$1.count > 1}
        let duplicates = Array(duplicateGroups.keys)
        return duplicates
    }
}

extension Array where Element == Double {
    
    func median() -> Double {
        let sortedArray = sorted()
        if count % 2 != 0 {
            return Double(sortedArray[count / 2])
        } else {
            return Double(sortedArray[count / 2] + sortedArray[count / 2 - 1]) / 2.0
        }
    }

    func sum() -> Double {
        
        return self.reduce(0, +)
    }
    
    func mean() -> Double {
        
        guard self.count > 0 else {
            return 0
        }
       
        return sum() / Double(self.count)
        
    }

}

/**
 var a = SynchronizedArray<Int>()
 a.append(1)
 a.append(2)
 a.append(3)
 
 // can be empty as this is non-thread safe access
 println(a.array)
 
 // thread-safe synchonized access
 println(a[0])
 println(a[1])
 println(a[2])
 **/
public class SynchronizedArray<T> {
    private var array: [T] = []
    private let accessQueue = DispatchQueue(label: "SynchronizedArrayAccess", attributes: .concurrent)
    
    public func append(newElement: T) {
        
        self.accessQueue.async(flags:.barrier) {
            self.array.append(newElement)
        }
    }
    
    public func removeAtIndex(index: Int) {
        
        self.accessQueue.async(flags:.barrier) {
            self.array.remove(at: index)
        }
    }
    
    public var count: Int {
        var count = 0
        
        self.accessQueue.sync {
            count = self.array.count
        }
        
        return count
    }
    
    public func first() -> T? {
        var element: T?
        
        self.accessQueue.sync {
            if !self.array.isEmpty {
                element = self.array[0]
            }
        }
        
        return element
    }
    
    public subscript(index: Int) -> T {
        set {
            self.accessQueue.async(flags:.barrier) {
                self.array[index] = newValue
            }
        }
        get {
            var element: T!
            self.accessQueue.sync {
                element = self.array[index]
            }
            
            return element
        }
    }
}

