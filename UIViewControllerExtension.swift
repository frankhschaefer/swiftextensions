//
//  UIViewExtension.swift
//  lapapp
//
//  Created by Frank Schäfer on 31.08.18.
//  Copyright © 2018 Frank Schäfer. All rights reserved.
//

import Foundation
import UIKit

// Put this piece of code anywhere you like
extension UIViewController {
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    /// Get the rootViewController on a thread safe way.
    var rootViewControllerThreadSafe : UIViewController? {
        
        var uiView : UIViewController? = UIViewController()
        
        UISync {
            uiView = self.view.window?.rootViewController
        }
        
        return uiView
    }
    

}
