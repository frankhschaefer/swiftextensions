//
//  AVCaptureDeviceExtension.swift
//  lapapp
//
//  Created by Frank Schäfer on 10.11.17.
//  Copyright © 2017 Frank Schäfer. All rights reserved.
//

import Foundation
import AVFoundation


extension AVCaptureDevice {


    /// Debug function
    func listAllSupportedAVFormats() {
        
        for format in self.formats {
            for frameRateRange in format.videoSupportedFrameRateRanges {
                        LoggerHandler.sharedLogger.log(lineNumber: #line,
                                           method: #function,
                                           file: #file,
                                           level: .info,
                                           tag: "POSITION",
                                           message:"format SFRR : \(format.formatDescription), frameRateRange \(frameRateRange.debugDescription)")
            }
        }
        
        for preset in AVCaptureSession().presets {
            LoggerHandler.sharedLogger.log(lineNumber: #line,
                                                     method: #function,
                                                     file: #file,
                                                     level: .info,
                                                     tag: "POSITION",
                                                     message:" preset \(preset), \(self.supportsSessionPreset(preset))")

        }
    }
    
    /// Get some information about the active video
    /// Return: Type used for video dimensions, a kind of CGRect in Int32
    func activeFormatDimenson() -> CMVideoDimensions {
    
        let formatDesc = self.activeFormat.formatDescription
        let dimensions = CMVideoFormatDescriptionGetDimensions(formatDesc)
    
        return dimensions
        
    }
    
    
    /// Search for the highst resolution from the device with the highest frame rate
    func bestFormatByMaxResolution() -> (format     : AVCaptureDevice.Format?,
                                         frameRate  : AVFrameRateRange?) {
        
        var bestFormat           : AVCaptureDevice.Format? = nil
        var bestResolution       : Int32 = 0
        var bestAVFrameRateRange : AVFrameRateRange? = nil
    
        for format in self.formats {
            if bestFormat == nil {
                bestFormat = format
                bestResolution = format.resolution()
            }
            
            
            if format.resolution() > bestResolution {
                bestFormat = format
                bestResolution = format.resolution()
                
                // best rate for this format
                bestAVFrameRateRange = nil
            }
            
            for range in format.videoSupportedFrameRateRanges {
                if bestAVFrameRateRange == nil {
                    bestAVFrameRateRange = range
                }
                
                if range.maxFrameRate > (bestAVFrameRateRange?.maxFrameRate)! {
                    bestAVFrameRateRange = range
                }
            }  // for
            
        }  // for
    
        return (bestFormat,bestAVFrameRateRange)
        
    }
    
    

    func getCaptureResolution() -> CGSize {
        // Define default resolution
        var resolution = CGSize(width: 0, height: 0)
        
        // Get video dimensions
        let formatDescription = self.activeFormat.formatDescription
        let dimensions = CMVideoFormatDescriptionGetDimensions(formatDescription)
        
        resolution = CGSize(width: CGFloat(dimensions.width), height: CGFloat(dimensions.height))
        
        // Return resolution
        return resolution
    }
    
}
