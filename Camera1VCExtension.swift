//
//  Camera1VCExtension.swift
//  lapapp
//
//  Created by Frank Schäfer on 21.03.18.
//  Copyright © 2018 Frank Schäfer. All rights reserved.
//

import Foundation


extension UIViewController: XMLParserDelegate {
    
    public func parser(_ parser: XMLParser,
                didStartElement elementName: String,
                namespaceURI: String?,
                qualifiedName qName: String?,
                attributes attributeDict: [String : String]) {
        
        debugPrint("Did start element: \(elementName)")
    }
    
    public func parser(_ parser: XMLParser,
                foundCharacters string: String) {
        
        debugPrint("Found characters: \(string)")
    }
    
    
    private func parser(parser: XMLParser,
                        didEndElement elementName: String,
                        namespaceURI: String?,
                        qualifiedName qName: String?) {
        
        debugPrint("Did end element: \(elementName)")
    }
    
    
    
    public func parserDidEndDocument(_ parser: XMLParser) {
        debugPrint("Completed parsing document")
    }
    
    
    
    func readXML() {
        if let filepath = Bundle.main.path(forResource: "measurement", ofType: "xml") {
            do {
                let contents = try String(contentsOfFile: filepath)
                let xmlData = contents.data(using: String.Encoding.utf8)!
                let parser = XMLParser(data: xmlData)
                
                parser.delegate = self
                
                guard parser.parse() else {
                    LoggerHandler.sharedLogger.log(lineNumber: #line,
                                  method: #function,
                                  file: #file,
                                  level: .warning,
                                  tag: "PROCESSING:DEBUG",
                                  message: "Failed parser.parse()")
                    return
                }
                
            } catch {
                // contents could not be loaded
            }
        } else {
            // example.txt not found!
        }
    }
    
    
    

}
