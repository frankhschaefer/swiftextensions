//
//  CGRectExtension.swift
//  lapapp
//
//  Created by Frank Schäfer on 06.06.17.
//  Copyright © 2017 Frank Schäfer. All rights reserved.
//

import Foundation


public func pythagoras(a:CGFloat, _ b:CGFloat) -> CGFloat {
  return sqrt(a*a + b*b)
}

struct CGRectSentence: Encodable {
    var count: String?
    var x: String?
    var y: String?
    var width: String?
    var height: String?
}


extension CGRect {

    func rectToSentence(count : Int) -> CGRectSentence
    {
        let sentence = CGRectSentence(count: String(count),
                                      x: self.minX.string(fractionDigits: 3),
                                      y: self.minY.string(fractionDigits: 3),
                                      width: self.width.string(fractionDigits: 3),
                                      height: self.height.string(fractionDigits: 3))
        return sentence
    }
    
    /// Rotate the size of the CGRect Width -> Height = X, Height -> Width = Y)
    func rotateSize() -> CGPoint{
        
        
        return CGPoint.init(x: self.size.height,
                            y: self.size.width)
        
    }
    
    /**
     Prints the values from the CGRect with some optional info text
     */
    func debugInfo(infoText : String? = nil) -> String {
        
        var message = ""
        
        if let theInfoText = infoText {
            message = "Info: \(theInfoText)"
        }
        
        message +=   ".x1 : " + "%2.2f".format(self.origin.x) +
                     ".x2 : " + "%2.2f".format(self.origin.x + self.size.width) +
                     ", .y1 : " + "%2.2f".format(self.origin.y) +
                     ", .y2 : " + "%2.2f".format(self.origin.y + self.size.height) +
                     ", wid.: " + "%2.2f".format(self.size.width) +
                     ", hei.: " + "%2.2f".format(self.size.height)
        
        
        return message
    }
    
    
    
    /**
     Prints the values from the CGRect with some optional info text
    */
    func printDebugInfo(infoText : String? = nil) {
        
               LoggerHandler.sharedLogger.log(lineNumber: #line,
                                               method: #function,
                                               file: #file,
                                               level: .info,
                                               tag: "POSITION",
                                               message:debugInfo(infoText: infoText))
    }
    
    
    /**
     Simple validation technique. Works only with rectangles that are oriented like the GUI views.
     This was necessary because of some data with not valid rects
     Change the size of the rect and do not change the center point

     */
    func validateAndHoldTheCenter(leftTop     : CGPoint,
                                  rightBottom : CGPoint) -> CGRect {
        
        var validatedLeftTop : CGPoint = CGPoint.init( x: self.minX,
                                                       y: self.minY)

        var validatedRightBottom : CGPoint = CGPoint.init( x: self.maxX,
                                                           y: self.maxY)

        
        validatedLeftTop = validatedLeftTop.validate(minX: leftTop.x,
                                                     maxX: rightBottom.x,
                                                     minY: leftTop.y,
                                                     maxY: rightBottom.y)

        validatedRightBottom = validatedRightBottom.validate(minX: leftTop.x,
                                                             maxX: rightBottom.x,
                                                             minY: leftTop.y,
                                                             maxY: rightBottom.y)
        
        
        return CGRect.init(x: validatedLeftTop.x,
                           y: validatedLeftTop.y,
                           width: abs(validatedLeftTop.x-validatedRightBottom.x),
                           height: abs(validatedLeftTop.y-validatedRightBottom.y))
        
    }

    
    
    /**
     Simple validation technique. Works only with rectangles that are oriented like the GUI views.
     This was nessesary becuase of some data with not valid rects
     Move the center of the rect and hold the size
     */
    func validateAndHoldTheSize(leftTop     : CGPoint,
                                rightBottom : CGPoint) -> CGRect {
        
        let validateAndHoldTheCenterRect = validateAndHoldTheCenter(leftTop: leftTop,
                                                                    rightBottom: rightBottom)
        
        if self == validateAndHoldTheCenterRect
        {
            return self
        }
        
        let offsetX = validateAndHoldTheCenterRect.width - self.width
        
        let offsetY = validateAndHoldTheCenterRect.height - self.height
        
        return CGRect.init(center: CGPoint.init(x: self.centerX + offsetX,
                                                y: self.centerY + offsetY),
                           size: CGSize.init(width: self.width,
                                             height: self.height))
    }

    
    /**
     Create a point which represents the center of the CGRect
     If the CGRect is invalid the func returns a zero point
     
     returns : Center of the CGRect
    */
    var centerPoint : CGPoint {
        get {
            return CGPoint.init(x: midX, y: midY)
        }
    }
    

    func centerAndAdjustPercentage(percentage pct: CGFloat) -> CGRect {
        
        return self.insetBy(dx:-self.width*pct,
                            dy:-self.height*pct)
    }


    func centerAndAdjustPercentage(percentageX pctX: CGFloat,
                                   percentageY pctY: CGFloat) -> CGRect {
        
        return self.insetBy(dx:-self.width*pctX,
                            dy:-self.height*pctY)
    }


    /**
     * Extend CGRect by CGPoint
     */
    mutating func union(withPoint: CGPoint) {
        if withPoint.x < self.origin.x { self.size.width += self.origin.x - withPoint.x; self.origin.x = withPoint.x }
        if withPoint.y < self.origin.y { self.size.height += self.origin.y - withPoint.y; self.origin.y = withPoint.y }
        if withPoint.x > self.origin.x + self.size.width { self.size.width = withPoint.x - self.origin.x }
        if withPoint.y > self.origin.y + self.size.height { self.size.height = withPoint.y - self.origin.y; }
    }
    
    /**
     * Get end point of CGRect
     */
    func maxPoint() -> CGPoint {
        return CGPoint( x : self.origin.x + self.size.width,
                        y : self.origin.y + self.size.height)
    }

    
    
    /**
     * CGRect
     *
     * var a = CGRectMake(30.4, 58.6, 20.3, 78.3)
     * var b = CGPointMake(-16.7, 40.5)
     * ...
     */
    
    /**
     * ...
     * a + b
     */
    static func + (left: CGRect, right: CGPoint) -> CGRect {
        return CGRect( x: left.origin.x + right.x,
                       y : left.origin.y + right.y,
                       width : left.size.width,
                       height : left.size.height)
    }
    
    /**
     * ...
     * a += b
     */
    static func += ( left: inout CGRect, right: CGPoint) {
        left = left + right
    }
    
    /**
     * ...
     * a - b
     */
    static func - (left: CGRect, right: CGPoint) -> CGRect {
        return CGRect( x : left.origin.x - right.x,
                       y : left.origin.y - right.y,
                       width : left.size.width,
                       height : left.size.height)
    }
    
    /**
     * ...
     * a -= b
     */
    static func -= ( left: inout CGRect, right: CGPoint) {
        left = left - right
    }
    
    /**
     * ...
     * a * 2.5
     */
    static func * (left: CGRect, right: CGFloat) -> CGRect {
        return CGRect( x: left.origin.x * right,
                       y : left.origin.y * right,
                       width : left.size.width * right,
                       height : left.size.height * right)
    }
    
    /**
     * ...
     * a *= 2.5
     */
    static func *= ( left: inout CGRect, right: CGFloat) {
        left = left * right
    }
    
    /**
     * ...
     * a / 4.0
     */
    static func / (left: CGRect, right: CGFloat) -> CGRect {
        return CGRect( x : left.origin.x / right,
                       y : left.origin.y / right,
                       width  : left.size.width / right,
                       height : left.size.height / right)
    }
    
    /**
     * ...
     * a /= 4.0
     */
    static func /= ( left: inout CGRect, right: CGFloat) {
        left = left / right
    }
    
    /** Creates a rectangle with the given center and dimensions
     - parameter center: The center of the new rectangle
     - parameter size: The dimensions of the new rectangle
     */
    init(center: CGPoint, size: CGSize)
    {
        self.init(x: center.x - size.width / 2, y: center.y - size.height / 2, width: size.width, height: size.height)
    }
    
    /** the coordinates of this rectangles center */
    var center: CGPoint
    {
        get { return CGPoint(x: centerX, y: centerY) }
        set { centerX = newValue.x; centerY = newValue.y }
    }
    
    /** the x-coordinate of this rectangles center
     - note: Acts as a settable midX
     - returns: The x-coordinate of the center
     */
    var centerX: CGFloat
    {
        get { return midX }
        set { origin.x = newValue - width * 0.5 }
    }
    
    /** the y-coordinate of this rectangles center
     - note: Acts as a settable midY
     - returns: The y-coordinate of the center
     */
    var centerY: CGFloat
    {
        get { return midY }
        set { origin.y = newValue - height * 0.5 }
    }
    
    // MARK: - "with" convenience functions
    
    /** Same-sized rectangle with a new center
     - parameter center: The new center, ignored if nil
     - returns: A new rectangle with the same size and a new center
     */
    func with(center: CGPoint?) -> CGRect
    {
        return CGRect(center: center ?? self.center, size: size)
    }
    
    /** Same-sized rectangle with a new center-x
     - parameter centerX: The new center-x, ignored if nil
     - returns: A new rectangle with the same size and a new center
     */
    func with(centerX: CGFloat?) -> CGRect
    {
        return CGRect(center: CGPoint(x: centerX ?? self.centerX, y: centerY), size: size)
    }
    
    /** Same-sized rectangle with a new center-y
     - parameter centerY: The new center-y, ignored if nil
     - returns: A new rectangle with the same size and a new center
     */
    func with(centerY: CGFloat?) -> CGRect
    {
        return CGRect(center: CGPoint(x: centerX, y: centerY ?? self.centerY), size: size)
    }
    
    /** Same-sized rectangle with a new center-x and center-y
     - parameter centerX: The new center-x, ignored if nil
     - parameter centerY: The new center-y, ignored if nil
     - returns: A new rectangle with the same size and a new center
     */
    func with(centerX: CGFloat?, centerY: CGFloat?) -> CGRect
    {
        return CGRect(center: CGPoint(x: centerX ?? self.centerX, y: centerY ?? self.centerY), size: size)
    }
    
    func leftTop() -> CGPoint
    {
        return CGPoint(x: self.minX, y: self.minY)
    }

    func rightTop() -> CGPoint
    {
        return CGPoint(x: self.maxX, y: self.minY)
    }

    func leftBottom() -> CGPoint
    {
        return CGPoint(x: self.minX, y: self.maxY)
    }

    func rightBottom() -> CGPoint
    {
        return CGPoint(x: self.maxX, y: self.maxY)
    }
    
    func allCornerPoints() -> [CGPoint]
    {
        return [leftTop(),
                rightTop(),
                rightBottom(),
                leftBottom(),
                leftTop()]
    }

    /// Length
    func length() -> CGFloat {
        return pythagoras(a: self.width,  self.height)
    }

    // Distance
    func distanceTo(other: CGPoint) -> CGFloat {
        return pythagoras(a: self.minX-other.x, self.minY-other.y)
    }
    
    
    /// Is the inner rect near to the rect
    /// Percentage is the distance to the rect
    func nearOuterRect(innerRect  : CGRect,
                       percentage : CGFloat) -> Bool
    {
        let distanceX = self.width * percentage
        let distanceY = self.height * percentage
        
        if innerRect.minX < distanceX {
            return true
        }

        if width - innerRect.maxX < distanceX {
            return true
        }

        if innerRect.minY < distanceY {
            return true
        }

        if height - innerRect.maxY < distanceY {
            return true
        }

        return false

    }
    
    var x1 : CGFloat
    {
        return self.minX
    }

    var x2 : CGFloat
    {
        return self.maxX
    }

    var y1 : CGFloat
    {
        return self.minY
    }

    var y2 : CGFloat
    {
        return self.maxY
    }

    
}  // CGRect Extension

extension CGRect {
    
    /// 90° oriented rectangle
    init?(pointCloud points: [CGPoint]) {
        let xArray = points.map(\.x)
        let yArray = points.map(\.y)
        if  let minX = xArray.min(),
            let maxX = xArray.max(),
            let minY = yArray.min(),
            let maxY = yArray.max() {

            self.init(x: minX,
                      y: minY,
                      width: maxX - minX,
                      height: maxY - minY)
        } else {
            return nil
        }
    }
    
    
}

