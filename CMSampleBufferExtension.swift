//
//  CMSampleBufferExtension.swift
//  lapapp
//
//  Created by Frank Schäfer on 11.08.18.
//  Copyright © 2018 Frank Schäfer. All rights reserved.
//

import Foundation
import AVFoundation

extension CMSampleBuffer {

    /**
     Write some information about the image output settings to the debug output
 
     Sampleoutput:
     availableImageDataCodecTypes                            : [jpeg]
     availableImageDataCVPixelFormatTypes                    : [875704422, 875704438, 1111970369]
     imageOutput.outputSettings.values.first?.description    : 1111970369
     imageOutput.outputSettings.keys.first?.debugDescription : PixelFormatType
 
     Background Info: 1111970369 = BRGA Images
 
     - parameter imageOutput: an key-value pair, filled with the image output settings
 
     print some information about the sampleBuffer to the debug output
 
     - parameter cmSampleBuffer: Image
     */
    func printCMSampleBuffer() {
    
               LoggerHandler.sharedLogger.log(lineNumber: #line,
                                           method: #function,
                                           file: #file,
                                           level: .info,
                                           tag: "POSITION",
                                           message:"CMSampleBufferDataIsReady              : \(CMSampleBufferDataIsReady(self))")
               LoggerHandler.sharedLogger.log(lineNumber: #line,
                                           method: #function,
                                           file: #file,
                                           level: .info,
                                           tag: "POSITION",
                                           message:"CMSampleBufferGetDataBuffer            : \(String(describing: CMSampleBufferGetDataBuffer(self)))")
               LoggerHandler.sharedLogger.log(lineNumber: #line,
                                           method: #function,
                                           file: #file,
                                           level: .info,
                                           tag: "POSITION",
                                           message:"CMSampleBufferGetDecodeTimeStamp       : \(CMSampleBufferGetDecodeTimeStamp(self))")
               LoggerHandler.sharedLogger.log(lineNumber: #line,
                                           method: #function,
                                           file: #file,
                                           level: .info,
                                           tag: "POSITION",
                                           message:"CMSampleBufferGetFormatDescription     : \(String(describing: CMSampleBufferGetFormatDescription(self)))")
               LoggerHandler.sharedLogger.log(lineNumber: #line,
                                           method: #function,
                                           file: #file,
                                           level: .info,
                                           tag: "POSITION",
                                           message:"CMSampleBufferGetNumSamples            : \(CMSampleBufferGetNumSamples(self))")
               LoggerHandler.sharedLogger.log(lineNumber: #line,
                                           method: #function,
                                           file: #file,
                                           level: .info,
                                           tag: "POSITION",
                                           message:"CMSampleBufferGetSampleSize            : \(CMSampleBufferGetSampleSize(self,at: 0))")
               LoggerHandler.sharedLogger.log(lineNumber: #line,
                                           method: #function,
                                           file: #file,
                                           level: .info,
                                           tag: "POSITION",
                                           message:"CMSampleBufferIsValid                  : \(CMSampleBufferIsValid(self))")
               LoggerHandler.sharedLogger.log(lineNumber: #line,
                                           method: #function,
                                           file: #file,
                                           level: .info,
                                           tag: "POSITION",
                                           message:"CMSampleBufferGetImageBuffer           : \(String(describing: CMSampleBufferGetImageBuffer(self)))")
               LoggerHandler.sharedLogger.log(lineNumber: #line,
                                           method: #function,
                                           file: #file,
                                           level: .info,
                                           tag: "POSITION",
                                           message:"kCGImagePropertyExifDictionary         : \(CMGetAttachment(self,key: kCGImagePropertyExifDictionary,attachmentModeOut: nil)!)")
        
        if let  exifAttachments = CMGetAttachment(self,key: kCGImagePropertyExifDictionary, attachmentModeOut: nil) {
                   LoggerHandler.sharedLogger.log(lineNumber: #line,
                                           method: #function,
                                           file: #file,
                                           level: .info,
                                           tag: "POSITION",
                                           message:"exifAttachments            : \(exifAttachments)")
        } else {
                   LoggerHandler.sharedLogger.log(lineNumber: #line,
                                           method: #function,
                                           file: #file,
                                           level: .info,
                                           tag: "POSITION",
                                           message:"exifAttachments NOT avalible")
        }
        if let pixelBuffer : CVImageBuffer = CMSampleBufferGetImageBuffer(self) {
                   LoggerHandler.sharedLogger.log(lineNumber: #line,
                                           method: #function,
                                           file: #file,
                                           level: .info,
                                           tag: "POSITION",
                                           message:"Image ok \(pixelBuffer)")
        } else {
                   LoggerHandler.sharedLogger.log(lineNumber: #line,
                                           method: #function,
                                           file: #file,
                                           level: .info,
                                           tag: "POSITION",
                                           message:"Image NOT ok")
        }
    
    }

}
