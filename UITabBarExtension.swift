//
//  tabbar.swift
//  lapapp
//
//  Created by Frank Schäfer on 15.10.17.
//  Copyright © 2017 Frank Schäfer. All rights reserved.
//

import Foundation
import UIKit


extension UITabBar {
    
 public func viewForTag(tag: Int) -> UIView? {
    
    return self.viewWithTag(tag)

}


}

