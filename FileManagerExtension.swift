//
//  FileManager.swift
//  lapapp
//
//  Created by Frank Schäfer on 22.04.18.
//  Copyright © 2018 Frank Schäfer. All rights reserved.
//

import Foundation

extension FileManager {

    /// Short-cut for the path documentDirectory
    class func documentDirectoryUrl() -> URL? {
        let paths = FileManager.default.urls(for: .documentDirectory,
                                             in: .userDomainMask)
        return paths.first
    }
    
    

    /// Short-cut for the path cachesDirectory
    class func cacheDirectoryUrl() -> URL? {
        let paths = FileManager.default.urls(for: .cachesDirectory,
                                             in: .userDomainMask)
        return paths.first
    }
    
    
    
    /// List of all the log-files
    /// Files are created by XCGLogger
    class func allLoggerData() -> [URL]? {
       if let theLoggerUrl = FileManager.cacheDirectoryUrl() {
          do {
              
                let directoryContents = try FileManager.default.contentsOfDirectory(at: theLoggerUrl,
                                                                                  includingPropertiesForKeys: nil)
            
                return directoryContents.filter{ $0.pathExtension == "log" }
          } catch {
              return nil
          }
       }
       return nil
    }
}

