
//
//  DispatchWorkItem.swift
//  lapapp
//
//  Created by Frank Schäfer on 18.11.17.
//  Copyright © 2017 Frank Schäfer. All rights reserved.
//
// Source: https://gist.github.com/boudhayan/e9f438d2a4955dadf508d40d7d0aab18

/*
 
 let queue = DispatchQueue(label: "TestQueue")
 
 DispatchWorkItem.run(on: queue, waitfor: .seconds(0)) {
    debugPrint("Please assign me tasks")
 }.notify(queue: DispatchQueue.main) {
    debugPrint("Tasks are done, Update UI")
 }
 
 */


import Foundation

public typealias DispatchBlock = () -> Swift.Void

public extension DispatchWorkItem {
    
    @discardableResult static func run(on queue: DispatchQueue? = nil, waitfor interval: DispatchTimeInterval? = nil, execute: @escaping DispatchBlock) -> DispatchWorkItem {
        let w = DispatchWorkItem(block: execute)
        if let waitInterval = interval { w.wait(waitInterval) }
        if let q = queue {
            q.async(execute: w)
        }else {
            w.perform()
        }
        return w
    }
    
    private func wait(_ interval: DispatchTimeInterval) {
        let _ =  self.wait(timeout: DispatchTime.now() + interval)
    }
    
    
}


