//
//  VNRequestExtension.swift
//  lapapp
//
//  Created by Frank Schäfer on 18.01.20.
//  Copyright © 2020 Frank Sch‚àö¬ßfer. All rights reserved.
//

import Foundation
import Vision
import CoreMedia

extension VNRequest
{
    private static var _mlImageForRequest = [String:MLImage]()
      
      var mlImageForRequest:MLImage {
          get {
              let tmpAddress = String(format: "%p", unsafeBitCast(self, to: Int.self))
            return VNRequest._mlImageForRequest[tmpAddress] ?? MLImage.init()
          }
          set(newValue) {
              let tmpAddress = String(format: "%p", unsafeBitCast(self, to: Int.self))
              VNRequest._mlImageForRequest[tmpAddress] = newValue
          }
      }
}

