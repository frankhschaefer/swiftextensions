//
//  AVCaptureVideoOrientationExtension.swift
//  lapapp
//
//  Created by Frank Schäfer on 16.09.18.
//  Copyright © 2018 Frank Schäfer. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit

/**
 
 USAGE:
 
 if let connection = cameraView.previewLayer?.connection {
    connection.videoOrientation = AVCaptureVideoOrientation(ui:self.interfaceOrientation)
 }
 
 if let connection = imageOutput.connection(withMediaType: AVMediaTypeVideo),
    connection.isVideoOrientationSupported,
    let orientation = AVCaptureVideoOrientation(orientation: UIDevice.current.orientation) {
    connection.videoOrientation = orientation
 }
 
*/
extension AVCaptureVideoOrientation {
    
    
    var uiInterfaceOrientation: UIInterfaceOrientation {
        get {
            switch self {
            case .landscapeLeft:        return .landscapeLeft
            case .landscapeRight:       return .landscapeRight
            case .portrait:             return .portrait
            case .portraitUpsideDown:   return .portraitUpsideDown
            @unknown default:
                return .portrait
            }
        }
    }
    
    
    var uiDeviceOrientation : UIDeviceOrientation
    {
        get {
            switch self {
            case .landscapeLeft:        return .landscapeLeft
            case .landscapeRight:       return .landscapeRight
            case .portrait:             return .portrait
            case .portraitUpsideDown:   return .portraitUpsideDown
            @unknown default:
                return .portrait
            }
        }
    }
    
    init(ui:UIInterfaceOrientation) {
        switch ui {
        case .landscapeRight:       self = .landscapeRight
        case .landscapeLeft:        self = .landscapeLeft
        case .portrait:             self = .portrait
        case .portraitUpsideDown:   self = .portraitUpsideDown
        default:                    self = .portrait
        }
    }
    
    init?(orientation:UIDeviceOrientation) {
        switch orientation {
        case .landscapeRight:       self = .landscapeLeft
        case .landscapeLeft:        self = .landscapeRight
        case .portrait:             self = .portrait
        case .portraitUpsideDown:   self = .portraitUpsideDown
        default:
            return nil
        }
    }
}
