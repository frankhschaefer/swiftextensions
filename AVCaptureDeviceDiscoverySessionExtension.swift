//
//  AVCaptureDeviceDiscoverySessionExtension.swift
//  lapapp
//
//  Created by Frank Schäfer on 06.05.17.
//  Copyright © 2017 Frank Schäfer. All rights reserved.
//

import Foundation
import AVFoundation

extension AVCaptureDevice.DiscoverySession {
    
    func uniqueDevicePositionsCount() -> Int {
        var uniqueDevicePositions = [AVCaptureDevice.Position]()
        
        for device in devices {
            if !uniqueDevicePositions.contains(device.position) {
                uniqueDevicePositions.append(device.position)
            }
        }
        
        return uniqueDevicePositions.count
    }
}

