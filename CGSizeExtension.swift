//
//  CGPointExtension.swift
//  lapapp
//
//  Created by Frank Schäfer on 17.12.17.
//  Copyright © 2017 Frank Schäfer. All rights reserved.
//

import Foundation



/**
 * CGSize
 */

extension CGSize {
    /**
     * var a = CGSizeMake(8.9, 14.5)
     * var b = CGSizeMake(20.4, 75.8)
     * ...
     */

    /**
     * ...
     * a + b
     */
    static func + (left: CGSize, right: CGSize) -> CGSize {
        return CGSize( width : left.width + right.width,
                       height : left.height + right.height)
    }

    /**
     * ...
     * a += b
     */
    static func += ( left: inout CGSize, right: CGSize) {
        left = left + right
    }

    /**
     * ...
     * a - b
     */
    static func - (left: CGSize, right: CGSize) -> CGSize {
        return CGSize(width  : left.width - right.width,
                      height : left.height - right.height)
        
    }

    /**
     * ...
     * a -= b
     */
    static func -= ( left: inout CGSize, right: CGSize) {
        left = left - right
    }

    /**
     * ...
     * a * b
     */
    static func * (left: CGSize, right: CGSize) -> CGSize {
        return CGSize(width  : left.width * right.width,
                      height : left.height * right.height)
    }

    /**
     * ...
     * a *= b
     */
    static func *= ( left: inout CGSize, right: CGSize) {
        left = left * right
    }

    /**
     * ...
     * a / b
     */
    static func / (left: CGSize, right: CGSize) -> CGSize {
        return CGSize(width  : left.width / right.width,
                      height : left.height / right.height)
    }

    /**
     * ...
     * a /= b
     */
    static func /= ( left: inout CGSize, right: CGSize) {
        left = left / right
    }

    /**
     * var c = CGPointMake(-3.5, -17.6)
     * ...
     */

    /**
     * ...
     * a + c
     */
    static func + (left: CGSize, right: CGPoint) -> CGSize {
        return CGSize(width  : left.width + right.x,
                      height : left.height + right.y)
    }

    /**
     * ...
     * a += c
     */
    static func += ( left: inout CGSize, right: CGPoint) {
        left = left + right
    }

    /**
     * ...
     * a - c
     */
    static func - (left: CGSize, right: CGPoint) -> CGSize {
        return CGSize(width  : left.width - right.x,
                      height : left.height - right.y)
    }

    /**
     * ...
     * a -= c
     */
    static func -= ( left: inout CGSize, right: CGPoint) {
        left = left - right
    }

    /**
     * ...
     * a * c
     */
    static func * (left: CGSize, right: CGPoint) -> CGSize {
        return CGSize( width  : left.width * right.x,
                       height : left.height * right.y)
    }

    /**
     * ...
     * a *= c
     */
    static func *= ( left: inout CGSize, right: CGPoint) {
        left = left * right
    }

    /**
     * ...
     * a / c
     */
    static func / (left: CGSize, right: CGPoint) -> CGSize {
        return CGSize(width  : left.width / right.x,
                      height : left.height / right.y)
    }

    /**
     * ...
     * a /= c
     */
    static func /= ( left: inout CGSize, right: CGPoint) {
        left = left / right
    }

    /**
     * ...
     * a * 4.6
     */
    static func * (left: CGSize, right: CGFloat) -> CGSize {
        return CGSize(width  : left.width * right,
                      height : left.height * right)
    }

    /**
     * ...
     * a *= 4.6
     */
    static func *= ( left: inout CGSize, right: CGFloat) {
        left = left * right
    }

    /**
     Phytagoras c2 = a2 + b2
    */
    var hypothenuse : CGFloat {
        get {
            return sqrt((self.width * self.width) + (self.height * self.height))
        }
    }
    
    var area : CGFloat
    {
        return abs(self.width * self.height)
    }

}
