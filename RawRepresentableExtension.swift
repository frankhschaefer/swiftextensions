//
//  RawRepresentableExtension.swift
//  lapapp
//
//  Created by Frank Schäfer on 10.08.18.
//  Copyright © 2018 Frank Schäfer. All rights reserved.
//

import Foundation



func <<T: RawRepresentable>(a: T, b: T) -> Bool where T.RawValue: Comparable {
    return a.rawValue < b.rawValue
}

func ><T: RawRepresentable>(a: T, b: T) -> Bool where T.RawValue: Comparable {
    return a.rawValue > b.rawValue
}
