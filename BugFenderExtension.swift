//
//  BugFenderExtension.swift
//  lapapp
//
//  Created by Frank Schäfer on 08.03.18.
//  Copyright © 2018 Frank Schäfer. All rights reserved.
//

import Foundation
import BugfenderSDK


/// Default = True, when the data about the device are send to the logger, this war will be change to false.
var deviceDataSendToBugFender = false

/// Default = True, when the data about the camera are send to the logger, this war will be change to false.
var cameraDataSendToBugFender = false
