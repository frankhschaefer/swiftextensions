
//
//  TimeIntervalExtension.swift
//  lapapp
//
//  Created by Frank Schäfer on 04.11.2018
//  Copyright © 2018 Frank Schäfer. All rights reserved.
//
import UIKit
import Foundation

extension TimeInterval {
    
    var formatted: String {
        let endingDate = Date()
        let startingDate = endingDate.addingTimeInterval(-self)
        let calendar = Calendar.current
        
        let componentsNow = calendar.dateComponents([.hour, .minute, .second],
                                                    from: startingDate,
                                                    to: endingDate)
        
        if let hour = componentsNow.hour, let minute = componentsNow.minute, let seconds = componentsNow.second {
            
            return "\(String(format: "%02d", hour)):\(String(format: "%02d", minute)):\(String(format: "%02d", seconds))"
            
        } else {
            
            return "00:00:00"
        }
    }
}

