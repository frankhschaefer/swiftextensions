//
//  DeviceExtension.swift
//  lapapp
//
//  Created by Frank Schäfer on 23.03.18.
//  Copyright © 2018 Frank Schäfer. All rights reserved.
//


import Foundation
import AVFoundation
import Device_swift

public extension UIDevice {

    var isPhone : Bool {
        get {

            return (DeviceType.iPhone2G.rawValue ... DeviceType.iPodTouch6G.rawValue).contains(deviceType.rawValue)

        }
    }

    
    var isPad : Bool {
        get {
            
            return   (DeviceType.iPad.rawValue ... DeviceType.iPadPro9Inch.rawValue).contains(deviceType.rawValue)
        
        }
    }

    
    var isBigPad : Bool {
        get {
            
            return   (DeviceType.iPadPro10p5Inch.rawValue ... DeviceType.iPadPro12Inch.rawValue).contains(deviceType.rawValue)
        
        }
    }

    /// When the device is laying on the "face" there is no measurement possible
    /// This leads to a removeal of all ARElements
    var positionOfTheDeviceValidForMeasurement : Bool
    {
        
            return UIDevice.current.orientation != .unknown ||
                   UIDevice.current.orientation.interfaceOrientation != .unknown
        
    } // positionOfTheDeviceValidForMeasurement
    
    
    /// get a stable device position
    var uiDevicePosition: UIDeviceOrientation
    {
        // Remove some code, see https://cst.redminehosting.de/issues/2675
        return UIScreen.main.uiDeviceOrientation
    }

}
