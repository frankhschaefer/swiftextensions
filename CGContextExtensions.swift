//
//  TouchHeatmapExtensions.swift
//  TouchHeatmap
//
//  Created by Christopher Helf on 27.09.15.
//  Copyright © 2015 Christopher Helf. All rights reserved.
//

import Foundation
import UIKit

/**

- UIViewController Extension
In order to make screenshots and to manage the flows between Controllers,
we need to know when a screen was presented. We override the initialization 
function here and exchange implementations of the viewDidAppear method
In addition, we can add a name to a UIViewController so it's name is being
tracked more easily

*/

extension CGContext {
    final func drawImage(image: CGImage,
                         inRect rect: CGRect) {

        //flip coords
        let ty: CGFloat = (rect.origin.y + rect.size.height)
        
        translateBy(x: 0, y: ty)
        scaleBy(x: 1.0, y: -1.0)

        //draw image
        let rect__y_zero = CGRect(x: rect.origin.x,
                                  y: 0,
                                  width: rect.width,
                                  height: rect.height)
        
        draw(image, in: rect__y_zero)

        //flip back
        scaleBy(x: 1.0, y: -1.0)
        translateBy(x: 0, y: -ty)

    }
}


