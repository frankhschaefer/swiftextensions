//
//  BoolExtension.swift
//  lapapp
//
//  Created by Frank Schäfer on 23.06.18.
//  Copyright © 2018 Frank Schäfer. All rights reserved.
//

import Foundation

extension Bool {
    init<T: BinaryInteger>(_ num: T) {
        self.init(num != 0)
    }
    
    var toStringWithFiveChars : String {
        get {
            if self {
                return "TRUE "
            } else {
                return "FALSE"
            }
        }
    }

    
    var toLocalString : String {
        get {
            if self {
                return NSLocalizedString("Yes",
                                         comment: "")
            } else {
                return NSLocalizedString("No",
                                         comment: "")
            }
        }
    }

}

