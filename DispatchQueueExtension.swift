//
//  DispatchQueueExtension.swift
//  lapapp
//
//  Created by Frank Schäfer on 18.11.17.
//  Copyright © 2017 Frank Schäfer. All rights reserved.
//

import Foundation

extension DispatchQueue {

    /// Sample: DispatchQueue.mainSyncSafe(execute: {self.activateProgressbar ()})
    ///
    class func mainSyncSafe(execute work: () -> Void) {
        if Thread.isMainThread {
            work()
        } else {
            DispatchQueue.main.sync(execute: work)
        }
    }

    
    class func mainSyncSafe<T>(execute work: () throws -> T) rethrows -> T {
        if Thread.isMainThread {
            return try work()
        } else {
            return try DispatchQueue.main.sync(execute: work)
        }
    }
    
    
    private static var _tokenTracker = [String]()
    
    public class func once(executeToken: String, block:()->Void) {
        objc_sync_enter(self);
        defer { objc_sync_exit(self) }
        
        if _tokenTracker.contains(executeToken) {
            return
        }
        
        _tokenTracker.append(executeToken)
        block()
    }
}


