//
//  UIScrollViewExtension.swift
//  lapapp
//
//  Created by Frank Schäfer on 13.05.18.
//  Copyright © 2018 Frank Schäfer. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

extension UIScreen {
    
    /**
     Based on the screen coordinateSpace, returns
     
     portrait
     portraitUpsideDown
     landscapeLeft
     landscapeRight
     unknown
     
     Returns UIInterfaceOrientation
    **/
    var uiInterfaceOrientation: UIInterfaceOrientation {
        let point = coordinateSpace.convert(CGPoint.zero, to: fixedCoordinateSpace)
        switch (point.x, point.y) {
        case (0, 0):
            return .portrait
        case let (x, y) where x != 0 && y != 0:
            return .portraitUpsideDown
        case let (0, y) where y != 0:
            return .landscapeLeft
        case let (x, 0) where x != 0:
            return .landscapeRight
        default:
            LoggerHandler.sharedLogger.log(lineNumber: #line,
                                           method: #function,
                                           file: #file,
                                           level: .error,
                                           tag: "PROCESSING:DEVICEPOSITION",
                                           message: "(2) The position of the device in undefined. UIDevice.current.orientation: \(UIDevice.current.orientation.rawValue), UIDevice.current.orientation.interfaceOrientation: \(UIDevice.current.orientation.interfaceOrientation.rawValue)")
            return .unknown
        }
    }
    
    var uiDeviceOrientation : UIDeviceOrientation {
        return AVCaptureVideoOrientation.init(ui:uiInterfaceOrientation).uiDeviceOrientation
    }
}

