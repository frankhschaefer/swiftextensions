//
//  DoubleExtension.swift
//  lapapp
//
//  Created by Frank Schäfer on 22.04.18.
//  Copyright © 2018 Frank Schäfer. All rights reserved.
//

import Foundation

// Create a string with currency formatting based on the device locale
//
extension Double {
    
    var asLocaleDecimalStyleWithThreeDigits : String {
        
        let formatter         = NumberFormatter()
        
        formatter.roundingMode = NumberFormatter.RoundingMode.halfUp

        formatter.numberStyle = NumberFormatter.Style.decimal

        formatter.minimumFractionDigits = 1
        formatter.maximumFractionDigits = 3

        formatter.locale      = Locale.current
        
        return formatter.string(from: self as NSNumber)!
        
    }

    
    var asLocalDecimalStyleWithOneDigit : String {
        
        let formatter         = NumberFormatter()
        
        formatter.roundingMode = NumberFormatter.RoundingMode.halfUp
        
        formatter.numberStyle = NumberFormatter.Style.decimal
        
        formatter.minimumFractionDigits = 1
        formatter.maximumFractionDigits = 1
        
        formatter.locale      = Locale.current

        return formatter.string(from: self as NSNumber)!
        
    }

    
    var asLocaleDecimalStyle    :   String {
        
        let formatter         = NumberFormatter()
        
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 1

        formatter.locale      = Locale.current
        
        return formatter.string(from: self as NSNumber)!
        
    }

    /// returns a String with n-digits
    func string(fractionDigits:Int) -> String {
            return String(format:"%.\(fractionDigits)f", self)
    }
    
    
    /**
     returns the value between the limits or the boarder values
    */
    public func limit(minimalValue : Double, maximalValue : Double) -> Double {
        if self < minimalValue {
            return minimalValue
        }
        if self > maximalValue {
            return maximalValue
        }
        return self
    }
    

    /**
     Set the value between the limits or the boarder values
     */
    public init (minimalValue : Double, value : Double, maximalValue : Double) {
        if value < minimalValue {
            self = minimalValue
            return
        }
        if value > maximalValue {
            self = maximalValue
            return
        }
        self = value
        
    }
    
    /// SwiftRandom extension
    public static func random(_ lower: Double = 0, _ upper: Double = 100) -> Double {
        return (Double(arc4random()) / 0xFFFFFFFF) * (upper - lower) + lower
    }
    
    /// Radian to degree = RAD * 180 / PI
    public func radToDegree() -> Double {
        return self * 180 / Double.pi
    }
    
    
    /// Radian to degree = RAD * 180 / PI
    public func degreeToRad() -> Double {
        return self *  Double.pi / 180
    }


    func round(to places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
    
    
    func roundedDecimal(fractionDigits: Int = 0) -> Double {
        
        let strValue = self.string(fractionDigits: fractionDigits)

        let value = (strValue as NSString).doubleValue
        
        return value
    }
}


