//
//  ErrorExtension.swift
//  lapapp
//
//  Created by Frank Schäfer on 02.09.18.
//  Copyright © 2018 Frank Schäfer. All rights reserved.
//

import Foundation

extension Error {
    var code: Int { return (self as NSError).code }
    var domain: String { return (self as NSError).domain }
}
