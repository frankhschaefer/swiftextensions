//
//  UITextFieldExtension.swift
//  lapapp
//
//  Created by Frank Schäfer on 19.06.18.
//  Copyright © 2018 Frank Schäfer. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    
    var count:Int {
        get{
            return self.text?.utf16.count ?? 0
        }
        
    }
    
    var active : Bool {
        get {
            return self.isUserInteractionEnabled
        }
        set {
            /// Button / Labels which are not active
            let deactiveAlpha                   : CGFloat   = 0.6
            /// Button / Labels which are active
            let activeAlpha                     : CGFloat   = 1.0
            
            self.isUserInteractionEnabled = newValue
            self.alpha  = newValue ? activeAlpha : deactiveAlpha
        }
    }

}
