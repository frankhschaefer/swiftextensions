//
//  CaseCountable.swift
//  lapapp
//
//  Created by Frank Schäfer on 20.08.17.
//  Copyright © 2017 Frank Schäfer. All rights reserved.
// Sorce: https://stackoverflow.com/questions/27094878/how-do-i-get-the-count-of-a-swift-enum



import Foundation


protocol CaseCountable: RawRepresentable {}

extension CaseCountable where RawValue == Int {
    static var count: RawValue {
        var i: RawValue = 0
        while let _ = self.init(rawValue: i) { i += 1 }
        return i
    }
}
