//
//  CIContextExtension.swift
//  lapapp
//
//  Created by Frank Schäfer on 18.08.17.
//  Copyright © 2017 Frank Schäfer. All rights reserved.
//

import Foundation
import UIKit

/// This works in iOS without crash
/// First solution (iOS 8) http://petercompernolle.com/108096113846/excbadaccess-when-using-coreimage
/// Second solution (iOS 9) https://forums.developer.apple.com/thread/17142
extension CIContext {
    
    func createSaveCGImage(_ ciImage: CIImage) -> CGImage? {

        return CIContext(options: nil).createCGImage(ciImage,
                                                     from: ciImage.extent)
    
    }
    
    func createCGImageFailsave(_ image:CIImage) -> CGImage {
        
        let width = Int(image.extent.width)
        let height = Int(image.extent.height)
        
        let rawData =  UnsafeMutablePointer<UInt8>.allocate(capacity: width * height * 4)
        
        render(image,
               toBitmap     : rawData,
               rowBytes     : width * 4,
               bounds       : image.extent,
               format       : CIFormat.RGBA8,
               colorSpace   : CGColorSpaceCreateDeviceRGB())
        
        let dataProvider = CGDataProvider(dataInfo  : nil,
                                          data      : rawData,
                                          size      : height * width * 4,
                                          releaseData : {(datainfo,data,size ) in data.deallocate() })
        
        return CGImage(width: width,
                       height: height,
                       bitsPerComponent: 8,
                       bitsPerPixel: 32,
                       bytesPerRow: width * 4,
                       space: CGColorSpaceCreateDeviceRGB(),
                       bitmapInfo: CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue),
                       provider: dataProvider!,
                       decode: nil,
                       shouldInterpolate: false,
                       intent: .defaultIntent)!
    }
    
}


