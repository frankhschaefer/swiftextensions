//
//  CGPointExtension.swift
//  lapapp
//
//  Created by Frank Schäfer on 17.12.17.
//  Copyright © 2017 Frank Schäfer. All rights reserved.
//

import Foundation
import UIKit


extension CGPoint {

    func validate(minX : CGFloat,
                  maxX : CGFloat,
                  minY : CGFloat,
                  maxY : CGFloat) -> CGPoint {
        
        let validatedX : CGFloat = self.x.validate(min: minX,
                                                   max: maxX)
        
        let validatedY : CGFloat = self.y.validate(min: minY,
                                                   max: maxY)
        
        return CGPoint.init(x: validatedX,
                            y: validatedY)
    
    }

    /**
    * CGPoint
    *
    * var a = CGPointMake(13.5, -34.2)
    * var b = CGPointMake(8.9, 22.4)
    * ...
    */

    /**
    * ...
    * a + b
    */
    static func + (left: CGPoint, right: CGPoint) -> CGPoint {
        return CGPoint( x: left.x + right.x,
                        y : left.y + right.y)
    }

    /**
    * ...
    * a += b
    */
    static func += ( left: inout CGPoint, right: CGPoint) {
            left = left + right
    }

    /**
     * ...
     * a -= b
     */
    static func - (left: CGPoint, right: CGPoint) -> CGPoint {
        return CGPoint( x : left.x - right.x,
                        y : left.y - right.y)
    }

    /**
     * ...
     * a -= b
     */
    static func -= ( left: inout CGPoint, right: CGPoint) {
        left = left - right
    }

    /**
     * ...
     * a * b
     */
    static func * (left: CGPoint, right: CGPoint) -> CGPoint {
        return CGPoint( x : left.x * right.x,
                        y : left.y * right.y)
    }

    /**
     * ...
     * a *= b
     */
    static func *= ( left: inout CGPoint, right: CGPoint) {
        left = left * right
    }

    /**
     * ...
     * a / b
     */
    static func / (left: CGPoint, right: CGPoint) -> CGPoint {
        return CGPoint( x : left.x / right.x,
                        y : left.y / right.y)
    }

    /**
     * ...
     * a /= b
     */
    static func /= ( left: inout CGPoint, right: CGPoint) {
        left = left / right
    }

    /**
     * ...
     * a * 10.4
     */
    static func * (left: CGPoint, right: CGFloat) -> CGPoint {
        return CGPoint( x : left.x * right,
                        y : left.y * right)
    }

    /**
     * ...
     * a *= 10.4
     */
    static func *= ( left: inout CGPoint, right: CGFloat) {
        left = left * right
    }

    /**
     * ...
     * a / 10.4
     */
    static func / (left: CGPoint, right: CGFloat) -> CGPoint {
        return CGPoint( x : left.x / right,
                        y : left.y / right)
    }

    /**
     * ...
     * a /= 10.4
     */
    static func /= ( left: inout CGPoint, right: CGFloat) {
        left = left / right
    }

    /**
     * var c = CGSizeMake(20.4, 75.8)
     * ...
     */

    /**
     * ...
     * a + c
     */
    static func + (left: CGPoint, right: CGSize) -> CGPoint {
        return CGPoint( x : left.x + right.width,
                        y : left.y + right.height)
    }

    /**
     * ...
     * a += c
     */
    static func += ( left: inout CGPoint, right: CGSize) {
        left = left + right
    }

    /**
     * ...
     * a - c
     */
    static func - (left: CGPoint, right: CGSize) -> CGPoint {
        return CGPoint( x : left.x - right.width,
                        y : left.y - right.height)
    }

    /**
     * ...
     * a -= c
     */
    static func -= ( left: inout CGPoint, right: CGSize) {
        left = left - right
    }

    /**
     * ...
     * a * c
     */
    static func * (left: CGPoint, right: CGSize) -> CGPoint {
        return CGPoint( x : left.x * right.width,
                        y : left.y * right.height)
    }

    /**
     * ...
     * a *= c
     */
    static func *= ( left: inout CGPoint, right: CGSize) {
        left = left * right
    }

    /**
     * ...
     * a / c
     */
    static func / (left: CGPoint, right: CGSize) -> CGPoint {
        return CGPoint( x : left.x / right.width,
                        y : left.y / right.height)
    }

    /**
     * ...
     * a /= c
     */
    static func /= ( left: inout CGPoint, right: CGSize) {
        left = left / right
    }

    /**
     * ...
     * -a
     */
    static prefix func - (left: CGPoint) -> CGPoint {
        return CGPoint( x : -left.x,
                        y : -left.y)
    }

    
    
    /**
     * Get point by rounding to nearest integer value
     */
    func integerPoint() -> CGPoint {
        return CGPoint(
            x : CGFloat(Int(self.x >= 0.0 ? self.x + 0.5 : self.x - 0.5)),
            y : CGFloat(Int(self.y >= 0.0 ? self.y + 0.5 : self.y - 0.5))
        )
    }


    
    /**
     * Get the distance and index from the element with the minimum x - distance of multiple points
     */
    func minXDistanceIndex (cgPoints: AtomicArray<CGPoint>) -> (CGFloat,Int?)
    {

        guard let b = cgPoints[safe: 0] else
        {
            return (0,nil)
        }
        
        var distanceX = abs(self.x - b.x)

        var returnIndex = 0

        // yes, the first element is calculated twice... i don't care
        for (index,point) in cgPoints.enumerated()
        {
            if distanceX > abs(self.x - point.x)
            {
                returnIndex = index
                distanceX = abs(self.x - point.x)
            }
        }
        
        return (distanceX, returnIndex)
    }

    
    
    /**
     * Get the distance and index from the element with the minimum x - distance of multiple points
     */
    func minXDistanceIndex (cgPoints: [CGPoint]) -> (CGFloat,Int?)
    {

        guard let b = cgPoints[safe: 0] else
        {
            return (0,nil)
        }
        
        var distanceX = abs(self.x - b.x)

        var returnIndex = 0

        // yes, the first element is calculated twice... i don't care
        for (index,point) in cgPoints.enumerated()
        {
            if distanceX > abs(self.x - point.x)
            {
                returnIndex = index
                distanceX = abs(self.x - point.x)
            }
        }
        
        return (distanceX, returnIndex)
    }
    
    
    
    /**
     * Get minimum x and y values of multiple points
     */
    func min(a: CGPoint, b: CGPoint, rest: CGPoint...) -> CGPoint {
        var p = CGPoint( x : Swift.min(a.x, b.x),
                         y : Swift.min(a.y, b.y))
        
        for point in rest {
            p.x = Swift.min(p.x, point.x)
            p.y = Swift.min(p.y, point.y)
        }
        
        return p
    }

    
    /**
     * Get maximum x and y values of multiple points
     */
    func max(a: CGPoint, b: CGPoint, rest: CGPoint...) -> CGPoint {
        var p = CGPoint(x : Swift.max(a.x, b.x),
                        y : Swift.max(a.y, b.y))
        
        for point in rest {
            p.x = Swift.max(p.x, point.x)
            p.y = Swift.max(p.y, point.y)
        }
        
        return p
    }

    
    
    /// Distance to the next point
    func distanceTo(cgPoint : CGPoint) -> CGFloat {
         return hypot(self.x - cgPoint.x,
                      self.y - cgPoint.y);
    }

    
    
    /// Vertical distance
    /// Poitive and negative values are possible
    func distanceToAlongYAxis(cgPoint : CGPoint) -> CGFloat {
        return self.y - cgPoint.y;
    }

    
    
    /// Vertical distance
    /// Poitive and negative values are possible
    func distanceToAlongXAxis(cgPoint : CGPoint) -> CGFloat {
        return self.x - cgPoint.x;
    }

    
    
    /// Both axis are zero
    var isZero : Bool {
        return self.x == 0 && self.y == 0
    }
    
    
    
    /// Translate the point until 0 is reached
    func translateLeftAndTopInView(x : CGFloat, y : CGFloat) -> CGPoint
    {
        var xMin = self.x - x
        if xMin < 0
        {
            xMin = 0
        }

        var yMin = self.y - y
        if yMin < 0
        {
            yMin = 0
        }
        
        return CGPoint.init(x: xMin,
                            y: yMin)
        
    }
    
    
    
    /// Gradient between two points
    func gradient(cgPoint : CGPoint) -> Double?
    {
        let dif = Double(cgPoint.y - self.y)
        
        guard dif == 0 else {
            return nil
        }
        
        return Double(cgPoint.x - self.x) / dif
        
    }
    
    
    
    /// Move the point in direction an angle
    func translate(offsetForPoint : CGFloat,
                   angle : CGFloat) -> CGPoint
    {
        
        let dxPoint = CGFloat(cos(angle)) * offsetForPoint
        
        let dyPoint = CGFloat(sin(angle)) * offsetForPoint

        let dxyPoint = CGPoint.init(x: dxPoint, y: dyPoint)

        return self + dxyPoint

    }
    
    
    
    func searchNearestPoint (pointCloud : [CGPoint]) -> CGPoint?
    {
        
        let markerSortedByPythagorus = pointCloud.sorted(by: {a,b in
            
            let distanceA = self.distanceTo(cgPoint: a)
            let distanceB = self.distanceTo(cgPoint: b)
            
            return distanceA < distanceB
            
        })
        
        return markerSortedByPythagorus.first
        
    }

    
    enum CGPointsPosition
    {
       case topMost
       case bottomMost
       case leftMost
       case rightMost
     }
    
     func findPoint(points: [CGPoint], position: CGPointsPosition) -> CGPoint? {
       var result: CGPoint?
       switch (position) {
       case .bottomMost:
         result = points.max { a, b in a.y < b.y }
       case .topMost:
         result = points.max { a, b in a.y > b.y }
       case .leftMost:
         result = points.max { a, b in a.x > b.x }
       case .rightMost:
         result = points.max { a, b in a.x < b.x }
       }
       return result
     }
    
}  // CGPoint
