//
//  UIViewExtension.swift
//  lapapp
//
//  Created by Frank Schäfer on 31.08.18.
//  Copyright © 2018 Frank Schäfer. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func viewIsAdded(view : UIView?) -> Bool {
        
        guard let theView = view else {
            return false
        }
            
        return self.subviews.contains(theView)
    }

    // Using a function since `var image` might conflict with an existing variable
    // (like on `UIImageView`)
    func toImage() -> UIImage {
        if #available(iOS 10.0, *) {
            let renderer = UIGraphicsImageRenderer(bounds: bounds)
            return renderer.image { rendererContext in
                layer.render(in: rendererContext.cgContext)
            }
        } else {
            UIGraphicsBeginImageContext(self.frame.size)
            self.layer.render(in:UIGraphicsGetCurrentContext()!)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return UIImage(cgImage: image!.cgImage!)
        }
    }
    
}
