//
//  tableViewExtension.swift
//  lapapp
//
//  Created by Frank Schäfer on 16.07.17.
//  Copyright © 2017 Frank Schäfer. All rights reserved.
//

import Foundation
import UIKit


extension UITableViewController {
    
    
    func tableViewScrollToBottom(_ animated: Bool) {
        
        let delay = 0.1 * Double(NSEC_PER_SEC)
        let time = DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC)
        
        DispatchQueue.main.asyncAfter(deadline: time, execute: {
            
            let numberOfSections = self.tableView.numberOfSections
            let numberOfRows = self.tableView.numberOfRows(inSection: numberOfSections-1)
            
            if numberOfRows > 0 {
                let indexPath = IndexPath(row: numberOfRows-1, section: (numberOfSections-1))
                self.tableView.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: animated)
            }
            
        })
    }
    
    
    
    func tableViewScrollToTop(_ animated: Bool) {
        
        let delay = 0.1 * Double(NSEC_PER_SEC)
        let time = DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC)
        
        DispatchQueue.main.asyncAfter(deadline: time, execute: {
            
            if (self.numberOfSections(in: self.tableView) > 0 ) {
                
                let top = IndexPath(row: Foundation.NSNotFound, section: 0);
                self.tableView.scrollToRow(at: top, at: UITableView.ScrollPosition.top, animated: animated);
            }
        })
        
        
    }
    

    
    
    
}
