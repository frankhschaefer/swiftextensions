//
//  CharacterExtension.swift
//  lapapp
//
//  Created by Frank Schäfer on 01.07.18.
//  Copyright © 2018 Frank Schäfer. All rights reserved.
//

import Foundation


extension Character {
    var unicodeScalarsValue: UInt32 {
        return String(self).unicodeScalars.first!.value
    }
}
