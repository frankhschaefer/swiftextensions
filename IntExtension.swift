//
//  IntExtension.swift
//  lapapp
//
//  Created by Frank Schäfer on 02.09.18.
//  Copyright © 2018 Frank Schäfer. All rights reserved.
//

import Foundation


public extension Int {
    /// SwiftRandom extension
    static func random(_ lower: Int = 0, _ upper: Int = 100) -> Int {
        return lower + Int(arc4random_uniform(UInt32(upper - lower + 1)))
    }

    func secondsToHoursMinutesSeconds() -> (Int, Int, Int) {
        return (self / 3600, (self % 3600) / 60, (self % 3600) % 60)
    }
}


protocol ToInt { func toInt() -> Int }

extension UInt:     ToInt { func toInt() -> Int { return Int(self) } }
extension Int8:     ToInt { func toInt() -> Int { return Int(self) } }
extension UInt8:    ToInt { func toInt() -> Int { return Int(self) } }
extension Int16:    ToInt { func toInt() -> Int { return Int(self) } }
extension UInt16:   ToInt { func toInt() -> Int { return Int(self) } }
extension Int32:    ToInt { func toInt() -> Int { return Int(self) } }
extension UInt32:   ToInt { func toInt() -> Int { return Int(self) } }
extension Int64:    ToInt { func toInt() -> Int { return Int(self) } }
extension UInt64:   ToInt { func toInt() -> Int { return Int(self) } }




func bitRep<T:ToInt>(_ value: T) -> String {
    var size: Int
    switch value {
    case is Int8,  is UInt8: size = 7
    case is Int16, is UInt16: size = 15
    case is Int32, is UInt32: size = 31
    case is Int64, is UInt64: size = 63
    default : size = 63
    }
    let n = value.toInt()
    var rep = ""
    // was     for (var c = size; c >= 0; c -= 1) {
    for c in size ... 0 {
        let k = n >> c
        if (k & 1) == 1 { rep += "1" } else { rep += "0" }
        if c%8 == 0 && c != 0 { rep += " " }
    }
    return rep
}
