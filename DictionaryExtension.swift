//
//  DictionaryExtension.swift
//  COpenSSL
//
//  Created by Frank Schäfer on 17.06.18.
//

import Foundation

extension Dictionary {
    mutating func update(other:Dictionary) {
        for (key,value) in other {
            self.updateValue(value, forKey:key)
        }
    }

    /// Get Key-Value Pair (tuple) at given index:
    subscript(i:Int) -> (key:Key,value:Value) {
            get {
                return self[index(startIndex, offsetBy: i)];
            }
        }
    
}


extension Dictionary where Value:Comparable {
    var sortedByValue:[(Key,Value)] {return Array(self).sorted{$0.1 < $1.1}}
}
extension Dictionary where Key:Comparable {
    var sortedByKey:[(Key,Value)] {return Array(self).sorted{$0.0 < $1.0}}
}

