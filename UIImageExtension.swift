//
//  UIImageExtension.swift
//  lapapp
//
//  Created by Frank Schäfer on 06.06.17.
//  Copyright © 2017 Frank Schäfer. All rights reserved.
//

import Foundation
import Photos

extension UIImage {
    
    
    func mergeImageData(imageData: Data,
                        with metadata: NSDictionary) -> Data
    {
        let source: CGImageSource = CGImageSourceCreateWithData(imageData as NSData, nil)!
        let UTI: CFString = CGImageSourceGetType(source)!
        let newImageData =  NSMutableData()
        let cgImage = UIImage(data: imageData)!.cgImage
        let imageDestination: CGImageDestination = CGImageDestinationCreateWithData((newImageData as CFMutableData), UTI, 1, nil)!
        CGImageDestinationAddImage(imageDestination, cgImage!, metadata as CFDictionary)
        CGImageDestinationFinalize(imageDestination)

        return newImageData as Data
    }
    
    
    func imageRotatedByDegrees(_ degrees:CGFloat) -> UIImage {
        // calculate the size of the rotated view's containing box for our drawing space
        let rotatedViewBox = UIView(frame: CGRect(x: 0,
                                                  y: 0,
                                                  width: self.size.width,
                                                  height: self.size.height))
        
        let t: CGAffineTransform = CGAffineTransform(rotationAngle: CGFloat(degrees.degreeToRad()))
        rotatedViewBox.transform = t
        let rotatedSize = rotatedViewBox.frame.size
        //[rotatedViewBox release];
        
        // Create the bitmap context
        UIGraphicsBeginImageContext(rotatedSize)
        
        guard let bitmap: CGContext = UIGraphicsGetCurrentContext() else {

            LoggerHandler.sharedLogger.log(lineNumber: #line,
                          method: #function,
                          file: #file,
                          level: .error,
                          tag: "HANDLEIMAGE",
                          message: "No context!")
            
            UIGraphicsEndImageContext()
            
            return self
        }
        
        // Move the origin to the middle of the image so we will rotate and scale around the center.
        bitmap.translateBy(x: rotatedSize.width/2,
                           y: rotatedSize.height/2)
        
        // Rotate the image context
        bitmap.rotate(by: CGFloat(degrees.degreeToRad()))
        
        // Now, draw the rotated/scaled image into the context
        bitmap.scaleBy(x: 1.0,
                       y: -1.0)
        
        bitmap.draw(self.cgImage!,
                    in: CGRect(x: -self.size.width / 2,
                               y: -self.size.height / 2,
                               width: self.size.width,
                               height: self.size.height))
        
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        
        UIGraphicsEndImageContext()
        
        return newImage
    }

    

    // Source: http://stackoverflow.com/questions/28906914/how-do-i-add-text-to-an-image-in-ios-swift
    /// Add an text to an UIImage
    /// Usage: uiImageWIthText = textToImage("000", inImage: UIImage(named:"thisImage.png")!, atPoint: CGPointMake(20, 20))
    func textToImage(_ drawText  : NSString,
                     atPoint     : CGPoint,
                     textColor   : UIColor = UIColor.magenta,
                     size        : CGFloat = 20.0 ) -> UIImage {
        
        // Setup the font specific variables
        let textFont    : UIFont  = UIFont(name : "Helvetica Bold",
                                           size : size)!
        
        //Setup the image context using the passed image.
        UIGraphicsBeginImageContext(self.size)
        
        //Setups up the font attributes that will be later used to dictate how the text should be drawn
        let textFontAttributes = [
            NSAttributedString.Key.font             : textFont,
            NSAttributedString.Key.foregroundColor  : textColor,
            ]
        
        //Put the image into a rectangle as large as the original image.
        self.draw(in: CGRect(x: 0,
                             y: 0,
                             width: self.size.width,
                             height: self.size.height))
        
        // Creating a point within the space that is as bit as the image.
        let rect: CGRect = CGRect(x: atPoint.x,
                                  y: atPoint.y,
                                  width: self.size.width,
                                  height: self.size.height)
        
        //Now Draw the text into an image.
        drawText.draw(in: rect,
                      withAttributes: textFontAttributes)
        
        // Create a new image out of the images we have created
        let uiImageWithText : UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        
        // End the context now that we have the image we need
        UIGraphicsEndImageContext()

        return uiImageWithText
        
    }

    
    /// Draws a grid over the image
    func drawGridToImage(offsetX: CGFloat,
                        offsetY: CGFloat,
                        lineWidth: CGFloat,
                        color: CGColor = UIColor.magenta.cgColor ) -> UIImage {
    
        let width = self.size.width
        let height = self.size.width

        guard width > offsetX * 2,
              height > offsetY * 2 else {
            return self
        }
        
        // Frame around the image
        var points : [CGPoint] = [CGPoint.init(x: 0,
                                               y: 0),
                                  CGPoint.init(x: width,
                                               y: 0),
                                  CGPoint.init(x: width,
                                               y: height),
                                  CGPoint.init(x: 0,
                                               y: height),
                                  CGPoint.init(x: 0,
                                               y: 0)]
        // Lines along the y-axis
        for xPosition in stride(from: 0,
                                through: width,
                                by: offsetY)
        {
            
            points.append(CGPoint.init(x: xPosition,
                                       y: 0))
            
            points.append(CGPoint.init(x: xPosition,
                                       y: height))
            
            points.append(CGPoint.init(x: xPosition,
                                       y: 0))

        }

        
        points.append(CGPoint.init(x: 0,
                                   y: 0))

        // Lines along the x-axis
        for yPosition in stride(from: 0,
                                through: height,
                                by: offsetY)
        {
            points.append(CGPoint.init(x: 0,
                                       y: yPosition))

            points.append(CGPoint.init(x: width,
                                       y: yPosition))

            points.append(CGPoint.init(x: 0,
                                       y: yPosition))
        }

        // Set the UIImageView image to the new image
        return drawFromPoints(points,
                              lineWidth : 2,
                              color     : color)

    }
    
    
    
    func drawRect(cgRect    : CGRect,
                  lineWidth : CGFloat,
                  color     : CGColor = UIColor.magenta.cgColor )  -> UIImage?
    {
    

        return drawFromPoints(cgRect.allCornerPoints(),
                              lineWidth: lineWidth,
                              color: color)
                                
    }
    
    
    /// Draws a line from point 1 to point 2 and so on in a UIImage
    func drawFromPoints(_ points    : [CGPoint],
                          lineWidth : CGFloat,
                          color     : CGColor = UIColor.magenta.cgColor ) -> UIImage {
        
        // No points, return start image
        if points.count < 2 {
            return self
        }
        
        // set the context to that of the uiimage
        UIGraphicsBeginImageContext(self.size)
        
        guard let theContext = UIGraphicsGetCurrentContext() else {
           
            LoggerHandler.sharedLogger.log(lineNumber: #line,
                          method: #function,
                          file: #file,
                          level: .error,
                          tag: "HANDLEIMAGE",
                          message: "UIGraphicsGetCurrentContext is nil")
            // free memory
            UIGraphicsEndImageContext()
            
            return self
        }
        
        
        // draw the existing image into the current context
        self.draw( in: CGRect(x      : 0,
                              y      : 0,
                              width  : self.size.width,
                              height : self.size.height))
        
        // draw the first new line segment
        theContext.setLineWidth(CGFloat(lineWidth))
        
        // set the color to magenta
        theContext.setStrokeColor(color)
        
        // start the drawing
        theContext.beginPath()
        
        // the first (start) point of the path
        theContext.move(to: CGPoint(x: points[0].x, y: points[0].y))
        
        // draw all other points to the path
        for pointNo in 1 ..< points.count {
            theContext.addLine(to: CGPoint(x: points[pointNo].x,
                                           y: points[pointNo].y))
        }
        
        // draw to the contect
        theContext.strokePath()
        
        // obtain a UIImage object from the context. Make a new UIImage
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        
        // free memory
        UIGraphicsEndImageContext()
        
        if newImage == nil {
            
            let infoText = "newImage == nil in \(#function)"
            
            LoggerHandler.sharedLogger.log(lineNumber: #line,
                                           method: #function,
                                           file: #file,
                                           level: .error,
                                           tag: "IMAGEHANDLING",
                                           message:infoText)
            
            fatalError(infoText)
        }
        
        
        // Set the UIImageView image to the new image
        return newImage!
    }
    
    

    /// Draws a circle from centerpoint with definend diameter in a UIImage
    func drawCircleFromCenter(center    : CGPoint,
                              diameter  : CGFloat,
                              lineWidth : CGFloat,
                              color     : CGColor = UIColor.magenta.cgColor ) -> UIImage {
        
        // No diameter, return start image
        if diameter <= 0 {
            return self
        }
        
        // set the context to that of the uiimage
        UIGraphicsBeginImageContext(self.size)
        
        guard let theContext = UIGraphicsGetCurrentContext() else {
            
            LoggerHandler.sharedLogger.log(lineNumber: #line,
                          method: #function,
                          file: #file,
                          level: .error,
                          tag: "HANDLEIMAGE",
                          message: "UIGraphicsGetCurrentContext is nil")
            
            // free memory
            UIGraphicsEndImageContext()
            
            // No context, return start image
            return self
        }
        
        
        // draw the existing image into the current context
        self.draw( in: CGRect(x      : 0,
                              y      : 0,
                              width  : self.size.width,
                              height : self.size.height))
        
        theContext.setFillColor(UIColor.clear.cgColor)
        
        theContext.setStrokeColor(color)
        
        theContext.setLineWidth(lineWidth)
        
        let rectangle = CGRect(x: center.x,
                               y: center.y,
                               width: diameter,
                               height: diameter)
        
        theContext.addEllipse(in: rectangle)
        
        theContext.drawPath(using: .fillStroke)
        
        // obtain a UIImage object from the context. Make a new UIImage
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        
        // free memory
        UIGraphicsEndImageContext()
        
        if newImage == nil
        {

            LoggerHandler.sharedLogger.log(lineNumber: #line,
                          method: #function,
                          file: #file,
                          level: .error,
                          tag: "HANDLEIMAGE",
                          message: "FatalError. newImage == nil")
            
            fatalError("FatalError. newImage == nil")
        }
        
        // Set the UIImageView image to the new image
        return newImage!
        
    }
    
    

    
    
    
    /// Create a UIImage with a new size
    func resize(to newSize: CGSize) -> UIImage {
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: newSize.width,
                                                      height: newSize.height),
                                                false,
                                                1.0)
        
        self.draw(in: CGRect(x: 0,
                             y: 0,
                             width: newSize.width,
                             height: newSize.height))
        
        let resizedImage = UIGraphicsGetImageFromCurrentImageContext()!
        
        UIGraphicsEndImageContext()
        
        return resizedImage
    }
    
    
    /**
     Generate a new Image in a new width

     - parameter newWidth: width for the new image
     - parameter opaque: renderFormat.opaque for the new image
     - parameter contentMode: UIViewContentMode, default .scaleAspectFit
     - returns: the new image
     */
    func resizeImage(_ dimension: CGFloat,
                     opaque: Bool,
                     contentMode: UIView.ContentMode = .scaleAspectFit) -> UIImage {
        
        var width: CGFloat
        var height: CGFloat
        var newImage: UIImage
        
        let size = self.size
        let aspectRatio =  size.width/size.height
        
        switch contentMode {
        case .scaleAspectFit:
            if aspectRatio > 1 {                            // Landscape image
                width = dimension
                height = dimension / aspectRatio
            } else {                                        // Portrait image
                height = dimension
                width = dimension * aspectRatio
            }
            
        default:
            let infoText = "UIIMage.resizeToFit(): FATAL: Unimplemented ContentMode in \(#function)"
            
                        LoggerHandler.sharedLogger.log(lineNumber: #line,
                                           method: #function,
                                           file: #file,
                                           level: .error,
                                           tag: "IMAGEHANDLING",
                                           message:infoText)
            
            fatalError(infoText)

        }
        
        if #available(iOS 10.0, *) {
            let renderFormat = UIGraphicsImageRendererFormat.default()
            renderFormat.opaque = opaque
            let renderer = UIGraphicsImageRenderer(size: CGSize(width: width, height: height), format: renderFormat)
            newImage = renderer.image {
                (context) in
                self.draw(in: CGRect(x: 0, y: 0, width: width, height: height))
            }
        } else {
            UIGraphicsBeginImageContextWithOptions(CGSize(width: width, height: height), opaque, 0)
            self.draw(in: CGRect(x: 0, y: 0, width: width, height: height))
            newImage = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
        }
        
        return newImage
    }
    

    /*
        let pixelBuffer = pixelBufferOut.memory!
        pixelBufferOut.destroy()
        return pixelBuffer
    
    */
    
    func pixelBuffer() -> CVPixelBuffer? {
        let width = self.size.width
        let height = self.size.height
        
        let attributes : [NSObject:AnyObject] = [
            kCVPixelBufferCGImageCompatibilityKey : true as AnyObject,
            kCVPixelBufferCGBitmapContextCompatibilityKey : true as AnyObject
        ]
        
        var pixelBuffer: CVPixelBuffer?
        
        let status = CVPixelBufferCreate(kCFAllocatorDefault,
                                         Int(width),
                                         Int(height),
                                         kCVPixelFormatType_32ARGB,
                                         attributes as CFDictionary,
                                         &pixelBuffer)
        
        guard let resultPixelBuffer = pixelBuffer else
        {
                    
            LoggerHandler.sharedLogger.log(lineNumber: #line,
                          method: #function,
                          file: #file,
                          level: .error,
                          tag: "HANDLEIMAGE",
                          message: "resultPixelBuffer failed at position 1, status \(status)")
            return nil
        }

        guard status == kCVReturnSuccess else
        {
            LoggerHandler.sharedLogger.log(lineNumber: #line,
                          method: #function,
                          file: #file,
                          level: .error,
                          tag: "HANDLEIMAGE",
                          message: "resultPixelBuffer failed at position 2, status \(status)")
            return nil
        }
        
        
        CVPixelBufferLockBaseAddress(resultPixelBuffer, CVPixelBufferLockFlags(rawValue: 0))
        
        let pixelData = CVPixelBufferGetBaseAddress(resultPixelBuffer)
        
        let rgbColorSpace = CGColorSpaceCreateDeviceRGB()
        
        guard let context = CGContext(data: pixelData,
                                      width: Int(width),
                                      height: Int(height),
                                      bitsPerComponent: 8,
                                      bytesPerRow: CVPixelBufferGetBytesPerRow(resultPixelBuffer),
                                      space: rgbColorSpace,
                                      bitmapInfo: CGImageAlphaInfo.noneSkipFirst.rawValue) else {
                                        
                                        LoggerHandler.sharedLogger.log(lineNumber: #line,
                                                      method: #function,
                                                      file: #file,
                                                      level: .error,
                                                      tag: "HANDLEIMAGE",
                                                      message: "CGContext failed, width\(width), height\(height), pixelData \(pixelData.debugDescription)")

                                        return nil
        }
        
        context.translateBy(x: 0, y: height)
        context.scaleBy(x: 1.0, y: -1.0)
        
        UIGraphicsPushContext(context)
        self.draw(in: CGRect(x: 0, y: 0, width: width, height: height))
        UIGraphicsPopContext()
        CVPixelBufferUnlockBaseAddress(resultPixelBuffer, CVPixelBufferLockFlags(rawValue: 0))
        
        return resultPixelBuffer
    }


    func saveToPhotoLibrarayDebug(okBlock: @escaping ()->Void,
                                  notOkBlock: @escaping (_ error : Error)->Void,
                                  everyTimeBlock: @escaping ()->Void) {
        
        
        PHPhotoLibrary.shared().performChanges({
            PHAssetChangeRequest.creationRequestForAsset(from: self)
        }, completionHandler: { success, error in
            if success {
                // Saved successfully!
                debugPrint("Image saved = success")

                okBlock()
            }
            else if let error = error {

                debugPrint("Image saved = NO success")
                notOkBlock(error)
                // Save photo failed with error
            }
            else {
                // Save photo failed with no error
            }
            debugPrint("Image saved, everyTime")

            everyTimeBlock()
        })
    }

        /// Return a new image cropped to a rectangle.
        /// - parameter rect:
        /// The rectangle to crop.
        open func cropped(to rect: CGRect) -> UIImage {
            // a UIImage is either initialized using a CGImage, a CIImage, or nothing
            if let cgImage = self.cgImage {
                // CGImage.cropping(to:) is magnitudes faster than UIImage.draw(at:)
                if let cgCroppedImage = cgImage.cropping(to: rect) {
                    return UIImage(cgImage: cgCroppedImage)
                } else {
                    return UIImage()
                }
            }
            if let ciImage = self.ciImage {
                // Core Image's coordinate system mismatch with UIKit, so rect needs to be mirrored.
                var ciRect = rect
                ciRect.origin.y = ciImage.extent.height - ciRect.origin.y - ciRect.height
                let ciCroppedImage = ciImage.cropped(to: ciRect)
                return UIImage(ciImage: ciCroppedImage)
            }
            return self
        }
 
    /**
     Create an empty UIImage of a given size, optionally filled with a given color.
     
     */
    convenience init? (size: CGSize,
                       filledWithColor color: UIColor = UIColor.clear,
                       opaque: Bool = false) {
        
        
        func imageWithSize(size: CGSize,
                           filledWithColor color: UIColor = UIColor.clear,
                           scale: CGFloat = 0.0,
                           opaque: Bool = false) -> CGImage? {
            
            let rect = CGRect.init(x: 0, y: 0, width: size.width, height: size.height)
            
            UIGraphicsBeginImageContextWithOptions(size, opaque, scale)
            
            color.set()
            
            UIRectFill(rect)
            
            guard let theUIGraphicsGetCurrentContext = UIGraphicsGetCurrentContext() else {
                return nil
            }
            
            let image = theUIGraphicsGetCurrentContext.makeImage()
            
            UIGraphicsEndImageContext()
            
            return image
        }
        
        
        guard let theCGImage = imageWithSize(size : size,
                                             filledWithColor: color,
                                             scale: 1.0,
                                             opaque: opaque) else {
            return nil
        }
        
        self.init(cgImage:theCGImage)
        
    }

    
    func getPixelColor(_ point: CGPoint) -> UIColor {
        guard let cgImage = self.cgImage else {
            return UIColor.clear
        }
        return cgImage.getPixelColor(point)
    }
    
    
}
