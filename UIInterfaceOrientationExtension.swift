//
//  UIInterfaceOrientationExtension.swift
//  lapapp
//
//  Created by Frank Schäfer on 06.05.17.
//  Copyright © 2017 Frank Schäfer. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit


extension UIInterfaceOrientation {
    var avCaptureVideoOrientation: AVCaptureVideoOrientation? {
        switch self {
        case .portrait: return .portrait
        case .portraitUpsideDown: return .portraitUpsideDown
        case .landscapeLeft: return .landscapeLeft
        case .landscapeRight: return .landscapeRight
        default: return nil
        }
    }
}
