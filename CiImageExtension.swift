//
//  ciImageExtension.swift
//  lapapp
//
//  Created by Frank Schäfer on 28.05.17.
//  Copyright © 2017 Frank Schäfer. All rights reserved.
//

import Foundation



extension CIImage {

    /**
     Rotate an CIImage with the fast filter functions
     
     - parameter ciImage: the image with should be rotated
     - parameter angle:   the angle, default is -90° - value in degree -
     
     - returns: the rotated CIImage
     */
    func rotate(angle : CGFloat = -90) -> CIImage {
    
        //Make CIFilter and set the CIImage
        let rotationCIFilter : CIFilter = CIFilter(name: "CIAffineTransform")!
        rotationCIFilter.setValue(self,forKey:"inputImage")
    
        let angleInRad = angle.degreeToRad()
        
        //Create NSValue of CGAffineTransform for a rotation of -90 degrees
        let rotation : NSValue = NSValue(cgAffineTransform:CGAffineTransform(rotationAngle:angleInRad))
    
        //Set the input transform value and get our output image
        rotationCIFilter.setValue(rotation, forKey: "inputTransform")
    
        let translationCIFilter : CIFilter = CIFilter(name: "CIAffineTransform")!
        translationCIFilter.setValue(rotationCIFilter.outputImage,forKey:"inputImage")
    
        //Create NSValue of CGAffineTransform for a translation, this moves the image in the visible range
        let translation : NSValue = NSValue(cgAffineTransform:CGAffineTransform(translationX: 0, y: self.extent.width))
    
        translationCIFilter.setValue(translation, forKey: "inputTransform")
    
        if translationCIFilter.outputImage == nil {
            LoggerHandler.sharedLogger.log(lineNumber: #line,
                                           method: #function,
                                           file: #file,
                                           level: .error,
                                           tag: "ROTIMAGE",
                                           message:"translationCIFilter.outputImage == nil")
            
            fatalError("translationCIFilter.outputImage == nil")
        }
        
        return  translationCIFilter.outputImage!
    
    }
    
    
    /**
     Rotate an CIImage with the fast filter functions so that the device orientation is corrected
     
     - returns: the rotated CIImage
     */
    func rotateAcordingToDeviceOrientation() -> CIImage {
        
        let deviceOrientation = UIDevice().uiDevicePosition

        guard deviceOrientation.videoOrientation != nil else {
            
            return self
            
        }
        
        switch (deviceOrientation) {

        case .unknown:
            break
        case .portrait: // Device oriented vertically, home button on the bottom
            return self.rotate()
        case .portraitUpsideDown: // Device oriented vertically, home button on the top
            return self.rotate(angle: 90) // angle: 90.0
        case .landscapeLeft: // Device oriented horizontally, home button on the right
            break
        case .landscapeRight: // Device oriented horizontally, home button on the left
            break // return self.rotate(angle: -180.0), removed 12.11.2017 - the visualisaton works now
        case .faceUp: // Device oriented flat, face up
            break
        case .faceDown: // Device oriented flat, face down
            break
        @unknown default:
            return self.rotate()
        }
        return self
    }
    
    
    /// to convert a CIImage to UIImage
    var uiImage : UIImage? {
        
        return UIImage.init(ciImage: self)
    }
   
    
    func saveImageWithDebugRect(text : String?,
                                cgRect : CGRect?)
    {

        guard var theUIDebugImage = self.uiImage else
        {
            return
        }
        
        if let theCGRect = cgRect
        {
           theUIDebugImage = theUIDebugImage.drawRect(cgRect: theCGRect,
                                                      lineWidth: 5) ?? theUIDebugImage
        }
        
        if let theText = text
        {
            let nsString = NSString.init(string: theText)
        
            theUIDebugImage = theUIDebugImage.textToImage(nsString,
                                                          atPoint: CGPoint.init(x: (theUIDebugImage.size.width / 10),
                                                                                y: (theUIDebugImage.size.height / 10)))
        }
        
        UIImageWriteToSavedPhotosAlbum(theUIDebugImage,
                                       self,
                                       #selector(self.image(_:didFinishSavingWithError:contextInfo:)),
                                                            nil)
        
    }

    //MARK: - Add image to Library
    @objc func image(_ image: UIImage,
                     didFinishSavingWithError error: Error?,
                     contextInfo: UnsafeRawPointer) {
        
        if let error = error {
            // we got back an error!
            debugPrint("Save error", error.localizedDescription)
        } else {
            debugPrint("Saved!", "Your image has been saved to your photos.")
        }
        
    }

    
}
