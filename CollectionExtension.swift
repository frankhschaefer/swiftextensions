//
//  collection.swift
//  lapapp
//
//  Created by Frank Schäfer on 13.07.17.
//  Copyright © 2017 Frank Schäfer. All rights reserved.
//

import Foundation
import UIKit


extension Collection {

    /**
    
     let arr = ["foo", "bar"]
     
     let str1 = arr[optional: 1] // --> str1 is now Optional("bar")
     
     if let str2 = arr[optional: 2] {
        debugPrint(str2) // --> this still wouldn't run
     } else {
        debugPrint("No string found at that index") // --> this would be printed
     }
     
    */
    subscript(safe index: Index) -> Iterator.Element? {
        
        if (index >= 0 as! Index && self.count as! Index > index) {
            // This line will not throw index out of range:
             return self[index]
        }

        return nil
       
    }
    
    
    // MARK: - Ints don't use the IntervalType. They use Range instead.
    // The benefit of doing this on the CollectionType type is that it's automatically carried over to the Dictionary and Array types.
    /**
     *  Examples:
     * (0...10).random()               // Ex: 6
     * ["A", "B", "C"].random()        // Ex: "B"
     * ["X":1, "Y":2, "Z":3].random()  // Ex: (.0: "Y", .1: 2)
     *
     *
     *  @return random element
     */
    public func random() -> Self.Element? {
        if let startIndex = self.startIndex as? Int {
            let start = UInt32(startIndex)
            let end = UInt32(self.endIndex as! Int)
            return self[Int(arc4random_uniform(end - start) + start) as! Self.Index]
        }
        var generator = self.makeIterator()
        var count = arc4random_uniform(UInt32(self.count))
        while count > 0 {
            _ = generator.next()
            count = count - 1
        }
        return generator.next()
    }
 
}


/**
 See: https://gist.github.com/BasThomas/d049f845d688dac5c06ca68af1f3ca1b
 usage:
 let numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
 let result = numbers.movingAverage(period: 10)
 
 let mapped = result
 .sorted { $0.key < $1.key }
 .map { $0.value }
 */
extension Collection where Element == Int, Index == Int {
    
    /// Calculates a moving average.
    /// - Parameter period: the period to calculate averages for.
    /// - Warning: the supplied `period` must be larger than 1.
    /// - Warning: the supplied `period` should not exceed the collection's `count`.
    /// - Returns: a dictionary of indexes and averages.
    func movingAverage(period: Int) -> [Int: Float] {
        precondition(period > 1)
        precondition(count > period)
        let result = (0..<self.count).compactMap { index -> (Int, Float)? in
            if (0..<period).contains(index) { return nil }
            let range = index - period..<index
            let sum = self[range].reduce(0, +)
            let result = Float(sum) / Float(period)
            
            return (index, result)
        }
        return Dictionary(uniqueKeysWithValues: result)
    }
}


extension Collection where Element == Double, Index == Int {
    
    /// Calculates a moving average.
    /// - Parameter period: the period to calculate averages for.
    /// - Warning: the supplied `period` must be larger than 1.
    /// - Warning: the supplied `period` should not exceed the collection's `count`.
    /// - Returns: a dictionary of indexes and averages.
    func movingAverage(period: Int) -> [Int: Double] {
        precondition(period > 1)
        precondition(count > period)
        let result = (0..<self.count).compactMap { index -> (Int, Double)? in
            if (0..<period).contains(index) { return nil }
            let range = index - period..<index
            let sum = self[range].reduce(0, +)
            let result = Double(sum) / Double(period)
            
            return (index, result)
        }
        return Dictionary(uniqueKeysWithValues: result)
    }
}


extension Collection where Element == CGPoint, Index == Int {
    
    /// Calculates a moving average.
    /// - Parameter period: the period to calculate averages for.
    /// - Warning: the supplied `period` must be larger than 1.
    /// - Warning: the supplied `period` should not exceed the collection's `count`.
    /// - Returns: a dictionary of indexes and averages.
    func movingAverage(period: Int) -> [Int: CGPoint] {
        
        precondition(period >= 1)
        
        precondition(count > period)
        
        let result = (0..<self.count).compactMap { index -> (Int, CGPoint)? in
            
            if (0..<period).contains(index) { return nil }
            
            let range = (index - period)...index

            
            let sumX = self[range].reduce(0,  {(first : Double, cgPoint: CGPoint) -> Double in
                return first + Double(cgPoint.x)
            })

            let sumY = self[range].reduce(0,  {(first : Double, cgPoint: CGPoint) -> Double in
                return first + Double(cgPoint.y)
            })

            let resultX = Double(sumX) / Double(range.count)

            let resultY = Double(sumY) / Double(range.count)

            return (index, CGPoint.init(x: resultX, y: resultY))
        
        }
        return Dictionary(uniqueKeysWithValues: result)
    }
}
