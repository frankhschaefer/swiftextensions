//
//  UIColorExtension.swift
//  lapapp
//
//  Created by Frank Schäfer on 06.11.17.
//  Copyright © 2017 Frank Schäfer. All rights reserved.
//

import Foundation
import UIKit

/** MARK: - Usage :
 
 let color = UIColor(red: 0.5, green: 0.8, blue: 0.8, alpha: 1.0)
 color.lighter(amount:0.5)
 color.darker(amount:0.5)
 OR (with the default values):
 
 color.lighter()
 color.darker()
 
 */
extension UIColor {
    
    func lighter(_ amount : CGFloat = 0.25) -> UIColor {
        return hueColorWithBrightnessAmount(1 + amount)
    }
    
    func darker(_ amount : CGFloat = 0.25) -> UIColor {
        return hueColorWithBrightnessAmount(1 - amount)
    }
    
    fileprivate func hueColorWithBrightnessAmount(_ amount: CGFloat) -> UIColor {
        var hue         : CGFloat = 0
        var saturation  : CGFloat = 0
        var brightness  : CGFloat = 0
        var alpha       : CGFloat = 0
        
        
        if getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: &alpha) {
            return UIColor( hue: hue,
                            saturation: saturation,
                            brightness: brightness * amount,
                            alpha: alpha )
        } else {
            return self
        }
        
    }
    
    
    /**
     This func generate 3 random numbers for RGB colour code (Red, Green, Blue) and
     then generating a UIColor object out of it.
     
     - returns: UIColor (random)
     */
    
    func randomUIColor() -> UIColor{
        
        let randomRed   :CGFloat = CGFloat(drand48())
        
        let randomGreen :CGFloat = CGFloat(drand48())
        
        let randomBlue  :CGFloat = CGFloat(drand48())
        
        return UIColor(red      : randomRed,
                       green    : randomGreen,
                       blue     : randomBlue,
                       alpha    : 1.0)
        
    }
    
    enum caaColors {
        case shade0
        case shade1
        case shade2
        case shade3
        case shade4
        case shade5
        case shade6
        case shade7
        case shade8
    }

    /**
     source : http://paletton.com/ for color 457CB5
     palette : http://paletton.com/palette.php?uid=13A0u0klsApbpR0gYIIpLvku5qx
     
     shade 0 = #457CB5 = rgb( 69,124,181) = rgba( 69,124,181,1) = rgb0(0.271,0.486,0.71)
     shade 1 = #99BDE3 = rgb(153,189,227) = rgba(153,189,227,1) = rgb0(0.6,0.741,0.89)
     shade 2 = #6899CC = rgb(104,153,204) = rgba(104,153,204,1) = rgb0(0.408,0.6,0.8)
     shade 3 = #2A65A2 = rgb( 42,101,162) = rgba( 42,101,162,1) = rgb0(0.165,0.396,0.635)
     shade 4 = #124D8A = rgb( 18, 77,138) = rgba( 18, 77,138,1) = rgb0(0.071,0.302,0.541)
255, 255, 255
    */
    func caaColorPalette(caaColor : caaColors) -> UIColor {
        
        switch caaColor {
        case .shade0:
            return UIColor.init(red: 0.271, green: 0.486, blue: 0.710, alpha: 1)
        case .shade1:
            return UIColor.init(red: 0.600, green: 0.741, blue: 0.890, alpha: 1)
        case .shade2:
            return UIColor.init(red: 0.408, green: 0.600, blue: 0.800, alpha: 1)
        case .shade3:
            return UIColor.init(red: 0.165, green: 0.396, blue: 0.635, alpha: 1)
        case .shade4:
            return UIColor.init(red: 0.071, green: 0.302, blue: 0.541, alpha: 1)
        case .shade5:
            return UIColor.init(red: 1.000, green: 1.000, blue: 0.541, alpha: 1)
        case .shade6:
            return UIColor.init(red: 0.039, green: 0.663, blue: 0.039, alpha: 1)
        case .shade7:
            return UIColor.init(red: 0.900, green: 0.900, blue: 0.000, alpha: 1)
        case .shade8:
            return UIColor.init(red: 0.756, green: 1.000, blue: 0.756, alpha: 1)
        }
    }
    
    var buttonColorBlue : UIColor {
        get {
            return caaColorPalette(caaColor: .shade0)
        }
    }

    var exposureColorYellow : UIColor {
        get {
            return caaColorPalette(caaColor: .shade5)
        }
    }
    
    var markerColorGreen : UIColor {
        get {
            return caaColorPalette(caaColor: .shade6)
        }
    }

    var markerColorYellow : UIColor {
        get {
            return caaColorPalette(caaColor: .shade7)
        }
    }
    
    var okLabelBackgrund : UIColor {
        get {
            return caaColorPalette(caaColor: .shade8)
        }
    }

    // MARK: - Initialization
    
    convenience init(hex: String, alpha: CGFloat = 1.0) {
        var hexFormatted: String = hex.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()

        if hexFormatted.hasPrefix("#") {
            hexFormatted = String(hexFormatted.dropFirst())
        }

        assert(hexFormatted.count == 6, "Invalid hex code used.")

        var rgbValue: UInt64 = 0
        Scanner(string: hexFormatted).scanHexInt64(&rgbValue)

        self.init(red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                  green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                  blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                  alpha: alpha)
    }
    
    
    
    // MARK: - Computed Properties
    
    var toHex: String? {
        return toHex()
    }
    
    // MARK: - From UIColor to String
    
    func toHex(alpha: Bool = false) -> String? {
        guard let components = cgColor.components, components.count >= 3 else {
            return nil
        }
        
        let r = Float(components[0])
        let g = Float(components[1])
        let b = Float(components[2])
        var a = Float(1.0)
        
        if components.count >= 4 {
            a = Float(components[3])
        }
        
        if alpha {
            return String(format: "%02lX%02lX%02lX%02lX", lroundf(r * 255), lroundf(g * 255), lroundf(b * 255), lroundf(a * 255))
        } else {
            return String(format: "%02lX%02lX%02lX", lroundf(r * 255), lroundf(g * 255), lroundf(b * 255))
        }
    }

}
