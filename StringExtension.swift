//
//  StringExtension.swift
//  lapapp
//
//  Created by Frank Schäfer on 11.06.17.
//  Copyright © 2017 Frank Schäfer. All rights reserved.
//

import Foundation

extension String {
    
    
    func contains(find: String) -> Bool {
        return self.range(of: find) != nil
    }
    
    
    func containsIgnoringCase(find: String) -> Bool {
        return self.range(of: find, options: NSString.CompareOptions.caseInsensitive) != nil
    }
    

    
    var asciiValue: UInt32 {
            guard let first = self.first, self.count == 1 else  { return 0 }
            return first.unicodeScalarsValue
        }

    
    func countInstances(of stringToFind: String) -> Int {
    
        var stringToSearch = self
        var count = 0
        
        while let foundRange = stringToSearch.range(of: stringToFind, options: .diacriticInsensitive) {
            stringToSearch = stringToSearch.replacingCharacters(in: foundRange, with: "")
            count += 1
        }
        return count
    }
    
    /// Get string between 2 strings in a string
    func slice(from : String,
               to   : String) -> String? {
        
        return (range(of: from)?.upperBound).flatMap { substringFrom in
            (range(of: to, range: substringFrom..<endIndex)?.lowerBound).map { substringTo in
                String(self[substringFrom..<substringTo])
            }
        }
    }
    
    /// Range subscript
    subscript(_ range: NSRange) -> String {
        let start = self.index(self.startIndex, offsetBy: range.lowerBound)
    
        let end = self.index(self.startIndex, offsetBy: range.upperBound)
        
        let subString = self[start..<end]
        
        return String(subString)
    }
    
    
    
    /// Remove n chars from the end of the string
    func removeCharsFromEnd(_ counter:Int) -> String {
        
        // This results in an empty string
        if counter >= self.count ||
           self.isEmpty {
            
            return ""
            
        }
        
        let start = self.index(self.startIndex,
                               offsetBy: 0)
        
        let end = self.index(self.startIndex,
                             offsetBy: self.count - counter)

        let subString = self[start..<end]
        
        return String(subString)
        
    }
    
    /// Remove n chars from the start of the string
    func removeCharsFromStart(_ counter:Int) -> String {
        
        // This results in an empty string
        if counter >= self.count ||
            self.isEmpty {
            
            return ""
            
        }
        
        let end = self.index(self.endIndex,
                               offsetBy: 0)
        
        let start = self.index(self.startIndex,
                             offsetBy: counter)
        
        let subString = self[start..<end]
        
        return String(subString)
        
    }

    
    /**
     see: http://rosettacode.org/wiki/Repeat_a_string#Swift
     usage print( "he".repeatString2(5) )
     
     benchmarked with a 1000 characters and 100 repeats this version is approx 500.000,00 times faster :-)
     
     - parameter n: n - repeats of the string
     
     - returns: String with n - repeats
     */
    func repeatString(_ n : Int) -> String {
        var result = self
        
        if n <= 0 {
           
            return ""
            
        } else if n == 1 {
            
            return result
        
        }
        
        for _ in 1 ... n - 1 {
        
            result.append(self)   // Note that String.extend is up to 10 times faster than "result += self"
            
        }
        
        return result
        
    }
    
    
    /// try to convert the string content to a double value
    func toDouble() -> Double? {
        return NSString(string: self).doubleValue
    }
 

   
    /// Strings like false, nein, off, not to false
    func toBool() -> Bool? {
            
        
        // vibration check - default ist off
        
        let stringFalseComparisionResult:ComparisonResult = self.localizedCaseInsensitiveCompare("false")
        
        let stringNeinComparisionResult:ComparisonResult = self.localizedCaseInsensitiveCompare("nein")
        
        let stringOffComparisionResult:ComparisonResult = self.localizedCaseInsensitiveCompare("off")
        
        let stringNoComparisionResult:ComparisonResult = self.localizedCaseInsensitiveCompare("no")
            
        
        // Result of the string compare is now available
        
        if self.isEmpty ||
            
            stringFalseComparisionResult == ComparisonResult.orderedSame ||
            
            stringNeinComparisionResult == ComparisonResult.orderedSame ||
           
            stringOffComparisionResult == ComparisonResult.orderedSame ||
            
            stringNoComparisionResult == ComparisonResult.orderedSame  {
            
            return false
           
        } else {
            
            return true
        }
    }
        

    
    

        
    func format(_ args: CVarArg...) -> String {
            
        return NSString(format: self, arguments: getVaList(args)) as String
            
    }

    
    // String to Date
    // Usage:  var withFormat = "12/01/2014".toDate(format: "MM/dd/yyyy")
    //         var usingDefaultFormat = "12/01/2014".toDate()
    func toDate(_ format:String = "dd.MM.yyyy") -> Date? {
            
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
            
        return dateFormatter.date(from: self)
    }
 
    
 
    func dateFromISOString() -> Date? {
        
        // "2016-11-01T21:10:56Z"
        let dateFormatter = ISO8601DateFormatter()
        
        return dateFormatter.date(from: self)
    }

        
    /**
        Find the index of an character in an string
         
         - parameter char: the needle which should be found
         
         - returns: position in the string or nil
    */
    func indexOfCharacter(_ char: Character) -> Int? {
        if let idx = self.firstIndex(of: char) {
            return self.distance(from: self.startIndex, to: idx)
        }
        return nil

    }
    
    
    
    /**
         Find the index of an character in an string
         
         - parameter char: the needle which should be found
         
         - parameter start: the start position for the search
         
         - returns: position in the string or nil
    */
    func indexOfNextCharacter(_ char             : Character,
                                indexStartOfText : Index) -> Index? {
            
            if self.isEmpty {
                return nil
            }
            
            if indexStartOfText >= self.endIndex {
                return nil
            }
        
            let range: Range = indexStartOfText..<self.endIndex
        
            let charSet = CharacterSet.init(charactersIn: String(char))
        
            guard let postionOfChar = self.rangeOfCharacter(from      : charSet,
                                                            options   : NSString.CompareOptions.literal,
                                                            range     : range) else {
                                                                
                return nil
            }
        
            return postionOfChar.upperBound
    }
    

    /**
     Extend or cut the string by some characters. If the string has the planed length, the func return the unchanged string.
     
     - parameter shouldHaveTheLengthOf: the planed length of the string
     
     - parameter fillchar: the string will be extend with this char until it reaches the planed length
     
     - returns: The extended or cut string

    */
    func withLength (shouldHaveTheLengthOf length : Int,
                     fillchar : Character = " ",
                     fillFromLeft : Bool = false) -> String {
        
        if self.count == length {
            
            return self
            
        } else if  self.count <= length {
            
            if fillFromLeft {
                return self + String(fillchar).repeatString(length - self.count )
            } else {
                return String(fillchar).repeatString(length - self.count ) + self
            }
            
        } else { // self.count > length
            
            if fillFromLeft {
                return self.removeCharsFromEnd(self.count - length)
            } else {
                return self.removeCharsFromStart(self.count - length)
            }
        }
        
    }  // func

    

    func asciiValueOfString() -> [UInt32] {
            
        var retVal = [UInt32]()
        for val in self.unicodeScalars where val.isASCII {
            retVal.append(UInt32(val))
        }
        return retVal
    }

    
    
    /**
     Usage:
     "abc".asciiArray            // [97, 98, 99]
     "abc".asciiArray[0]         // 97
     "abc".asciiArray[1]         // 98
     "abc".asciiArray[2]         // 99
    */
    var asciiArray: [UInt32] {
        return unicodeScalars.filter{$0.isASCII}.map{$0.value}
    }

    
    var trimmed:String {
        return self.trimmingCharacters(in: CharacterSet.whitespaces)
    }
    
    
    
    /// Return the string without all control chars
    var removingControlCharacters: String {
        return components(separatedBy: .controlCharacters).joined()
    }
    
    
    /// Return the string without spaces and new line chars
    var trimSpaceAndNewLine : String {
          return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    /// Returns a String without the forbidden chars like CR/LF, / ? and so on
    /// The String is limited to 30 chars
    var removeUnsupportedCharactersForFileName : String {
        
        var cleanString = self
        
        ["?", "/", "\\", "*", "CON", "PRN", "AUX", "CLOCK$", "NUL",
         "COM0", "COM1", "COM2", "COM3", "COM4", "COM5", "COM6", "COM7", "COM8", "COM9",
         "LPT0", "LPT1", "LPT2", "LPT3", "LPT4", "LPT5", "LPT6", "LPT7", "LPT8", "LPT9",
         ":", "|", "<", ">"].forEach {
            cleanString = cleanString.replacingOccurrences(of: $0, with: " ")
        }

        ["  ", "--"].forEach {
            cleanString = cleanString.replacingOccurrences(of: $0, with: " ")
        }
        
        return cleanString.removingControlCharacters.truncate(length: 50,
                                                              trailing: "_")
    }
    
    
    
    /**
     Truncates the string to the specified length number of characters and appends an optional trailing string if longer.
     
     - Parameter length: A `String`.
     - Parameter trailing: A `String` that will be appended after the truncation.
     
     - Returns: A `String` object.
     */
    func truncate(length: Int,
                  trailing: String = "…") -> String {
        
        if self.utf8.count > length {
            return String(self.prefix(length)) + trailing
        } else {
            return self
        }
    }

    
    
    /**
     Convert a Swift String to a C++ String
     */
    func toPointer() -> UnsafePointer<UInt8>? {
        guard let data = self.data(using: String.Encoding.utf8) else { return nil }
        
        let buffer = UnsafeMutablePointer<UInt8>.allocate(capacity: data.count)
        let stream = OutputStream(toBuffer: buffer, capacity: data.count)
        
        stream.open()
        data.withUnsafeBytes({ (p: UnsafePointer<UInt8>) -> Void in
            stream.write(p, maxLength: data.count)
        })
        
        stream.close()
        
        return UnsafePointer<UInt8>(buffer)
    }
    
    
    func isValidPhone() -> Bool {
            let phoneRegex = "^[0-9+]{0,1}+[0-9]{5,16}$"
            let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
            return phoneTest.evaluate(with: self)
        }

    
    
    func isValidEmail() -> Bool {
            let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
            let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
            return emailTest.evaluate(with: self)
        }

    /// Generates a `UIImage` instance from this string using a specified
    /// attributes and size.
    ///
    /// - Parameters:
    ///     - attributes: to draw this string with. Default is `nil`.
    ///     - size: of the image to return.
    /// - Returns: a `UIImage` instance from this string using a specified
    /// attributes and size, or `nil` if the operation fails.
    ///
    /// var image =
    ///     "Test".image(withAttributes: [
    ///         .foregroundColor: UIColor.red,
    ///         .font: UIFont.systemFont(ofSize: 30.0),
    ///     ], size: CGSize(width: 300.0, height: 80.0))

    /// Or
    /// image = "Test".image(withAttributes: [.font: UIFont.systemFont(ofSize: 80.0)])

    /// Or
    /// image = "Test".image(size: CGSize(width: 300.0, height: 80.0))

    /// Or even just
    /// image = "Test".image()
    
    func image(withAttributes attributes: [NSAttributedString.Key: Any]? = nil, size: CGSize? = nil) -> UIImage? {
        let size = size ?? (self as NSString).size(withAttributes: attributes)
        return UIGraphicsImageRenderer(size: size).image { _ in
            (self as NSString).draw(in: CGRect(origin: .zero, size: size),
                                    withAttributes: attributes)
        }
    }


    
    func numberOfLines() -> Int {
        return self.numberOfOccurrencesOf(string: "\n")
    }

    func numberOfOccurrencesOf(string: String) -> Int {
        return self.components(separatedBy:string).count - 1
    }
    
}
