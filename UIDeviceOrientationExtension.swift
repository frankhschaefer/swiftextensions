//
//  UIDeviceOrientationExtension.swift
//  lapapp
//
//  Created by Frank Schäfer on 06.05.17.
//  Copyright © 2017 Frank Schäfer. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit

extension UIDeviceOrientation {
    
    public var videoOrientation: AVCaptureVideoOrientation? {
        switch self {
        case .portrait: return .portrait
        case .portraitUpsideDown: return .portraitUpsideDown
        case .landscapeLeft: return .landscapeLeft
        case .landscapeRight: return .landscapeRight
        default: return nil
        }
    }
    
    /*
     case unknown
     The orientation of the device cannot be determined.
     
     case portrait
     The device is in portrait mode, with the device held upright and the home button at the bottom.
     
     case portraitUpsideDown
     The device is in portrait mode but upside down, with the device held upright and the home button at the top.
     
     case landscapeLeft
     The device is in landscape mode, with the device held upright and the home button on the right side.
     
     case landscapeRight
     The device is in landscape mode, with the device held upright and the home button on the left side.
     
     case faceUp
     The device is held parallel to the ground with the screen facing upwards.
     
     case faceDown
     The device is held parallel to the ground with the screen facing downwards.
     
     */
    public var interfaceOrientation: UIInterfaceOrientation {
        switch self {
        case .unknown: return .portrait
        case .portrait: return .portrait
        case .portraitUpsideDown: return .portraitUpsideDown
        case .landscapeLeft: return .landscapeLeft
        case .landscapeRight: return .landscapeRight
        case .faceUp: return .portrait
        case .faceDown: return .portrait
        @unknown default:
            return .portrait
        }
    }
    
    
  }
